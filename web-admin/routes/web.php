<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::name('admin.')->middleware('auth')->group(function () {
    Route::get('/', function () {
        return view('pages.dashboard');
    });

//    Route::get('/profile','UserController');

    Route::name('slider.')->group(function () {
        Route::get('/slider', 'SliderController@index')->name('list');
        Route::get('/slider/add', 'SliderController@viewForm')->name('add');
        Route::post('/slider/add', 'SliderController@store')->name('add');
        Route::get('/slider/edit/{id}', 'SliderController@viewForm')->name('edit');
        Route::post('/slider/edit/{id}', 'SliderController@update')->name('edit');
        Route::get('/slider/delete/{id}','SliderController@delete')->name('delete');
    });

    Route::name('update.')->group(function () {
        Route::get('/update', 'UpdateController@index')->name('list');
        Route::get('/update/add', 'UpdateController@viewForm')->name('add');
        Route::post('/update/add', 'UpdateController@store')->name('add');
        Route::get('/update/edit/{id}', 'UpdateController@viewForm')->name('edit');
        Route::post('/update/edit/{id}', 'UpdateController@update')->name('edit');
        Route::get('/update/delete/{id}','UpdateController@delete')->name('delete');
    });

    Route::name('news.')->group(function () {
        Route::get('/news', 'NewsController@index')->name('list');
        Route::get('/news/add', 'NewsController@viewForm')->name('add');
        Route::post('/news/add', 'NewsController@store')->name('add');
        Route::get('/news/edit/{id}', 'NewsController@viewForm')->name('edit');
        Route::post('/news/edit/{id}', 'NewsController@update')->name('edit');
        Route::get('/news/delete/{id}','NewsController@delete')->name('delete');
    });

    Route::name('testimonial.')->group(function () {
        Route::get('/testimonial', 'TestimonialController@index')->name('list');
        Route::get('/testimonial/add', 'TestimonialController@viewForm')->name('add');
        Route::post('/testimonial/add', 'TestimonialController@store')->name('add');
        Route::get('/testimonial/edit/{id}', 'TestimonialController@viewForm')->name('edit');
        Route::post('/testimonial/edit/{id}', 'TestimonialController@update')->name('edit');
        Route::get('/testimonial/delete/{id}','TestimonialController@delete')->name('delete');
    });

    Route::name('galleries.')->group(function () {
        Route::get('/galleries', 'GalleryController@index')->name('list');
        Route::get('/galleries/add', 'GalleryController@viewForm')->name('add');
        Route::post('/galleries/add', 'GalleryController@store')->name('add');
        Route::get('/galleries/edit/{id}', 'GalleryController@viewForm')->name('edit');
        Route::post('/galleries/edit/{id}', 'GalleryController@update')->name('edit');
        Route::get('/galleries/delete/{id}','GalleryController@delete')->name('delete');
    });

});

//    Route::resource('social', 'NewsController');

/*
web route
*/

Route::get('','WebController@home');
Route::get('/index','WebController@home');
Route::get('/applyNow','WebController@applyNow');
Route::get('/kanfedMeritScholarship','WebController@kanfedMeritScholarship');
Route::get('/Curriculum','WebController@Curriculum');
Route::get('/admissionDetails','WebController@admissionDetails');
Route::get('/lisfordInternational','WebController@lisfordInternational');
Route::get('/visionandMission','WebController@visionandMission');
Route::get('/promoters','WebController@promoters');
Route::get('/rulesandRegulations','WebController@rulesandRegulations');
Route::get('/sendMessage','WebController@sendMessage');
Route::get('/academicsEthos','WebController@academicsEthos');
Route::get('/addonCourses','WebController@addonCourses');
Route::get('/reportingandProgress','WebController@reportingandProgress');
Route::get('/results','WebController@results');
Route::get('/library','WebController@library');
Route::get('/departments','WebController@departments');
Route::get('/coCurricular','WebController@coCurricular');
Route::get('/socialResponsibility','WebController@socialResponsibility');
Route::get('/career','WebController@career');
Route::get('/gallery','WebController@gallery');
Route::get('/newsandEvents','WebController@newsandEvents');
Route::get('/contactUs','WebController@contactUs');
Route::get('/prayerandSchoolSong','WebController@prayerandSchoolSong');
Route::get('/feeStructure','WebController@feeStructure');


