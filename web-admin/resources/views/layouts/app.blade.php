 <!DOCTYPE html>
 <html>
 <head>
    @include('includes.head')
 </head>

 <body>
     <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
         @include('includes.sidebar')
        </nav>

         <div id="page-wrapper" class="gray-bg dashbard-1">
             <div class="row border-bottom">
                @include('includes.header')
             </div>
             <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper wrapper-content">
                        @yield('content')
                    </div>

                </div>
             </div>
             <div class="row">
                 <div class="col-lg-12">
                      <div class="footer">
                          @include('includes.footer')
                      </div>
                 </div>
              </div>

         </div>
     </div>

    @include('includes.scripts')
    @yield ('custom-scripts')
 </body>
 </html>
