<!DOCTYPE html>
<html>

<head>

	 @include('includes.head')

</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
	<div>
		<div>
			<h1 style="font-size: 85px;" class="logo-name">Lisford</h1>
		</div>
		<p>Login in. To see it in action.</p>
		<form class="m-t" method="POST" action="{{ route('login') }}">
			{{ csrf_field() }}
			<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
				<input type="text" class="form-control" placeholder="Username" required="" name="email" value="{{old('email')}}">
			</div>

			<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
				<input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

				@if($errors->any())
					<h4>{{$errors->first()}}</h4>
				@endif
			</div>
			<button type="submit" class="btn btn-primary block full-width m-b">Login</button>
			{{--<a href="{{ route('password.request') }}"><small>Forgot password?</small></a>--}}

		</form>

	</div>
</div>

<!-- Mainly scripts -->
@include('includes.scripts')
    @yield ('custom-scripts')}

</body>

</html>


