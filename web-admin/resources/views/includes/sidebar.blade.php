<div class="sidebar-collapse">
 <ul class="nav metismenu" id="side-menu">
     <li class="nav-header">
         <div class="dropdown profile-element"> <span>
             <img alt="image" class="img-circle" src="img/profile_small.jpg" />
              </span>
             <a data-toggle="dropdown" class="dropdown-toggle" href="#">
             <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Lisford International School</strong>
              </span> <span class="text-muted text-xs block">Admin <b class="caret"></b></span> </span> </a>
             <ul class="dropdown-menu animated fadeInRight m-t-xs">
                 {{--<li><a href="#">Profile</a></li>--}}
                 {{--<li><a href="contacts.html">Contacts</a></li>--}}
                 {{--<li><a href="mailbox.html">Mailbox</a></li>--}}
                 {{--<li class="divider"></li>--}}
                 <li><a onclick="document.getElementById('myform').submit();"><i class="zmdi zmdi-power"></i><span>Log Out</span></a>
                     							    <form id="myform" method="POST" action="/logout">@csrf</form></li>
             </ul>
         </div>
         <div class="logo-element">
             IN+
         </div>
     </li>
     <li class="active">
          <a href="{{route('admin.slider.list')}}"><i class="fa fa-image"></i> <span class="nav-label">Sliders</span></a>
     </li>
      <li>
          <a href="{{route('admin.update.list')}}"><i class="fa fa-bullhorn"></i> <span class="nav-label">Latest Updates</span></a>
      </li>
     <li>
          <a href="{{route('admin.testimonial.list')}}"><i class="fa fa-comments-o"></i> <span class="nav-label">Testimonial</span></a>
     </li>
     <li>
         <a href="{{route('admin.news.list')}}"><i class="fa fa-vcard-o"></i> <span class="nav-label">News and Events</span></a>
     </li>
      <li>
          <a href="{{route('admin.galleries.list')}}"><i class="fa fa-camera"></i> <span class="nav-label">Gallery</span></a>
      </li>
     {{--<li>--}}
         {{--<a href="{{route('admin.social.index',['type' => 'social'])}}"><i class="fa fa-diamond"></i> <span class="nav-label">Social</span></a>--}}
     {{--</li>--}}

     {{--<li>--}}
         {{--<a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Graphs</span><span class="fa arrow"></span></a>--}}
         {{--<ul class="nav nav-second-level collapse">--}}
             {{--<li><a href="graph_flot.html">Flot Charts</a></li>--}}
             {{--<li><a href="graph_morris.html">Morris.js Charts</a></li>--}}
             {{--<li><a href="graph_rickshaw.html">Rickshaw Charts</a></li>--}}
             {{--<li><a href="graph_chartjs.html">Chart.js</a></li>--}}
             {{--<li><a href="graph_chartist.html">Chartist</a></li>--}}
             {{--<li><a href="c3.html">c3 charts</a></li>--}}
             {{--<li><a href="graph_peity.html">Peity Charts</a></li>--}}
             {{--<li><a href="graph_sparkline.html">Sparkline Charts</a></li>--}}
         {{--</ul>--}}
     {{--</li>--}}
 </ul>

</div>