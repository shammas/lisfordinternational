
    <div class="pull-right text-right">
        Powered by <a href="http://www.cloudbery.com/" target="_blank"><img style="width: 33%;" src="http://www.cloudbery.com/cloudberyweb/assets/img/logo.png" alt="cloudbery solutions"/></a>
    </div>
    <div>
        <strong>Copyright</strong> Lisford International &copy; {{date("Y",strtotime("-1 year")).'-'.date("Y")}}
    </div>

