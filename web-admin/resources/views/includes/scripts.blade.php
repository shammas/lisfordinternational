<!-- Mainly scripts -->
 <script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
 <script src="{{asset('js/bootstrap.min.js')}}"></script>
 <script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
 <script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

 <!-- Custom and plugin javascript -->
 <script src="{{asset('js/inspinia.js')}}"></script>
 <script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>

 <script src="{{asset('js/custom.js')}}"></script>

 <!-- jQuery UI -->
 <script src="{{asset('js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

 <!-- Toastr -->
 <script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>

  <!-- Sweet alert -->
 <script src="{{asset('js/plugins/sweetalert/sweetalert.min.js')}}"></script>

 <script>
    function readURL(input,previewControl) {

       if (input.files && input.files[0]) {
         var reader = new FileReader();

         reader.onload = function(e)
         {
           $('#'+previewControl).attr('src', e.target.result);
         };

         reader.readAsDataURL(input.files[0]);
       }
     }


     $('#removeThis').click(function (e) {
         e.preventDefault();
         var url= $(this).attr("href");
         swal({
             title: "Are you sure?",
             text: "You will not be able to recover this data!",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Yes, delete it!",
             closeOnConfirm: false
         }, function () {
             window.location=url;

         });
     });

    @if(session()->has('smsg'))
        swal("{{session()->get('heading')}}", '{{session()->get('smsg')}}', "success");
    @endif

    @if(session()->has('emsg'))
        swal("{{session()->get('heading')}}", '{{session()->get('smsg')}}', "error");
    @endif

     function validateDimension(input, ele = '', width = 0, height = 0) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var image = new Image();
                image.src = e.target.result;

                image.onload = function () {
                    if (width != 0 && height != 0) {
                        if (this.width > width || this.height > height) {
                            var $el = $(input);
                            $el.wrap('<form>').closest('form').get(0).reset();
                            $el.unwrap();
                            swal("Warning", 'Selected image was too long. Please select this ' + width + ' x ' + height + ' ratio.', "error");
                            $('#' + ele).attr('src', '');
                            $('.fileinput-filename').html('');
                        } else {
                            $('#' + ele).attr('src', e.target.result);
                            $('#file-helper-text').append('your selected image '+this.width+'w and '+this.height+'h');
                        }
                    }
                };

            };

            reader.readAsDataURL(input.files[0]);

        }
    }

 </script>


 @yield('include-script')