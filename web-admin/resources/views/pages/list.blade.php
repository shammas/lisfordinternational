@section('include-style')
    <link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/dropzone/basic.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/dropzone/dropzone.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
@stop

@extends('layouts.app')
@section('title', 'slider')

@section('content')

    <div class="row" >
        <div class="col-lg-5">
            <div class="ibox float-e-margins" style="display: none" id="addSection">
                <div class="ibox-title">
                    <h5>Add slider</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                <form action="" class="form-horizontal"  enctype="multipart/form-data" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Name</label>
                        <div class="col-lg-10">
                            <input type="text" placeholder="Name" class="form-control" name="name" id="name">
                            <span class="help-block m-b-none">Enter your slider name if any.</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Name</label>
                        <div class="col-lg-10 dropzone" id="sliderDrop">
                            <div class="fallback" >
                                <input name="file" type="file"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-10 text-center">
                            <button class="btn btn-success" type="submit" id="submit-all">Submit</button>
                            <button class="btn btn-danger" type="button" id="cancelBtn">Cancel</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
                </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>List all sliders</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                        <th>$</th>
                            <th>Name</th>
                            <th>Preview</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sliders as $key => $slider)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$slider->name}}</td>
                            <td>{{$slider->image_path}}</td>
                            <td class="center">
                                <div class="btn-group btn-group-xs" role="group">
                                {{--<a class="btn btn-info" id="editSlider" href="{{route('admin.slider.edit',$slider->id)}}"><i class="fa fa-pencil"></i></a>--}}
                                    <a class="btn btn-danger" id="removeThis" href="{{route('admin.slider.delete',$slider->id)}}"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('include-script')
    <script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>

    <!-- Jasny -->
    <script src="{{asset('js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>

    <!-- DROPZONE -->
    <script src="{{asset('js/plugins/dropzone/dropzone.js')}}"></script>
@stop
@section('custom-scripts')
<script>
    $(document).ready(function(){
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>gl<"#add-btn.col-xs-12 col-sm-3 col-md-3 text-center">ft<"col-sm-3"i>p',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ],
            "initComplete": function(settings, json) {
//                        var myEl = document.getElementById('add-btn');
                var html = '<button  class="btn btn-primary btn-sm" type="button" onclick="newSlider()"><i class="fa fa-plus"></i>Add New Slider</button>';


//                        myEl.html(html);

                 $("#add-btn")
                         .html(html);
              }
        });
    });

    Dropzone.options.sliderDrop= {
        url: 'slider/add',
        autoProcessQueue: false,
        uploadMultiple: false,
        parallelUploads: 1,
        maxFiles: 1,
        maxFilesize: 2,
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        init: function() {
            dzClosure = this; // Makes sure that 'this' is understood inside the functions below.

            // for Dropzone to process the queue (instead of default form behavior):
            document.getElementById("submit-all").addEventListener("click", function(e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                dzClosure.processQueue();
            });

            //send all the form data along with the files:
            this.on("sending", function(data, xhr, formData) {
                formData.append("name", jQuery("#name").val());
            });
        },
        success: function(file, response){
            location.reload();
        }
    };

    function newSlider(){
        $('#addSection').show();
    }

    $('#cancelBtn').click(function(e) {
        $('#addSection').hide();
    });
</script>

@stop