@section('include-style')
    <link href="{{asset('css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">
@stop

@extends('layouts.app')
@section('title', 'update')

@section('content')
    <div class="row" >
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add update</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form action="" class="form-horizontal"  enctype="multipart/form-data" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" name="type" value="update"/>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Content <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <textarea class="form-control" name="content" id="content">@isset($update){{$update->content}}@endisset</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-10 text-center">
                                <button class="btn btn-success" type="submit" id="submit-all">Submit</button>
                                <a class="btn btn-danger" href="{{route('admin.update.list')}}">Cancel</a>
                            </div>
                        </div>
                    </form>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>


@stop
@section('include-script')
    <script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>
    <!-- Jasny -->
    <script src="{{asset('js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>
@stop
@section('custom-scripts')
<script>
    $(document).ready(function(){
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>gl<"#add-btn.col-xs-12 col-sm-3 col-md-3 text-center">ft<"col-sm-3"i>p',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ],
            "initComplete": function(settings, json) {
                var html = '<a class="btn btn-primary btn-sm" type="button" href="{{route('admin.update.add')}}" ><i class="fa fa-plus"></i>Add New Update</a>';
                 $("#add-btn")
                         .html(html);
              }
        });
    });

     $("#select_image").change(function() {
//      readURL(this,'preview-image');
      validateDimension(this,'preview-image',500,500);
    });
     $("#fileinput-exists").click(function() {
        $('#preview-image').attr('src', '{{asset('img/no-preview-available.png')}}');
    });



</script>

@stop