@section('include-style')
    <link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
@stop

@extends('layouts.app')
@section('title', 'update')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>List all updates</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th>$</th>
                            <th>Content</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($update as $key => $value)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$value->content}}</td>
                            <td class="center">
                                <div class="btn-group btn-group-xs" role="group">
                                    <a class="btn btn-info" id="editUpdate" href="{{route('admin.update.edit',$value->id)}}"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-danger" id="removeThis" href="{{route('admin.update.delete',$value->id)}}"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('include-script')
    <script src="js/plugins/dataTables/datatables.min.js"></script>
@stop
@section('custom-scripts')
<script>
    $(document).ready(function(){
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>gl<"#add-btn.col-xs-12 col-sm-3 col-md-3 text-center">ft<"col-sm-3"i>p',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ],
            "initComplete": function(settings, json) {
                var html = '<a class="btn btn-primary btn-sm" type="button" href="{{route('admin.update.add')}}" ><i class="fa fa-plus"></i>Add New Update</a>';
                 $("#add-btn")
                         .html(html);
              }
        });
    });



</script>

@stop