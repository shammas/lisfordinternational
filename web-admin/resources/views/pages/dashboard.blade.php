 @extends('layouts.app')
 @section('content')
     <div class="row  border-bottom white-bg dashboard-header">
          <div class="col-md-3">
              <h2>Welcome Lisford</h2>
              <small>You have 42 messages and 6 notifications.</small>
              <ul class="list-group clear-list m-t">
                  <li class="list-group-item fist-item">
                      <span class="pull-right">
                          09:00 pm
                      </span>
                      <span class="label label-success">1</span> Please contact me
                  </li>
                  <li class="list-group-item">
                      <span class="pull-right">
                          10:16 am
                      </span>
                      <span class="label label-info">2</span> Sign a contract
                  </li>
                  <li class="list-group-item">
                      <span class="pull-right">
                          08:22 pm
                      </span>
                      <span class="label label-primary">3</span> Open new shop
                  </li>
                  <li class="list-group-item">
                      <span class="pull-right">
                          11:06 pm
                      </span>
                      <span class="label label-default">4</span> Call back to Sylvia
                  </li>
                  <li class="list-group-item">
                      <span class="pull-right">
                          12:00 am
                      </span>
                      <span class="label label-primary">5</span> Write a letter to Sandra
                  </li>
              </ul>
          </div>

  </div>

  <div class="row">
       <div class="col-lg-12">
           <div class="wrapper wrapper-content">
               <div class="row">

               </div>
           </div>
           {{--<div class="footer">--}}
              {{--@include('includes.footer')--}}
          {{--</div>--}}
       </div>
  </div>
 @stop

 @section('custom-scripts')
  <script>
      $(document).ready(function() {
          setTimeout(function() {
              toastr.options = {
                  closeButton: true,
                  progressBar: true,
                  showMethod: 'slideDown',
                  timeOut: 4000
              };
              toastr.success('Responsive Admin Theme', 'Welcome to INSPINIA');

          }, 1300);


          var data1 = [
              [0,4],[1,8],[2,5],[3,10],[4,4],[5,16],[6,5],[7,11],[8,6],[9,11],[10,30],[11,10],[12,13],[13,4],[14,3],[15,3],[16,6]
          ];
          var data2 = [
              [0,1],[1,0],[2,2],[3,0],[4,1],[5,3],[6,1],[7,5],[8,2],[9,3],[10,2],[11,1],[12,0],[13,2],[14,8],[15,0],[16,0]
          ];
          $("#flot-dashboard-chart").length && $.plot($("#flot-dashboard-chart"), [
              data1, data2
          ],
                  {
                      series: {
                          lines: {
                              show: false,
                              fill: true
                          },
                          splines: {
                              show: true,
                              tension: 0.4,
                              lineWidth: 1,
                              fill: 0.4
                          },
                          points: {
                              radius: 0,
                              show: true
                          },
                          shadowSize: 2
                      },
                      grid: {
                          hoverable: true,
                          clickable: true,
                          tickColor: "#d5d5d5",
                          borderWidth: 1,
                          color: '#d5d5d5'
                      },
                      colors: ["#1ab394", "#1C84C6"],
                      xaxis:{
                      },
                      yaxis: {
                          ticks: 4
                      },
                      tooltip: false
                  }
          );

          var doughnutData = {
              labels: ["App","Software","Laptop" ],
              datasets: [{
                  data: [300,50,100],
                  backgroundColor: ["#a3e1d4","#dedede","#9CC3DA"]
              }]
          } ;


          var doughnutOptions = {
              responsive: false,
              legend: {
                  display: false
              }
          };


          var ctx4 = document.getElementById("doughnutChart").getContext("2d");
          new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});

          var doughnutData = {
              labels: ["App","Software","Laptop" ],
              datasets: [{
                  data: [70,27,85],
                  backgroundColor: ["#a3e1d4","#dedede","#9CC3DA"]
              }]
          } ;


          var doughnutOptions = {
              responsive: false,
              legend: {
                  display: false
              }
          };


          var ctx4 = document.getElementById("doughnutChart2").getContext("2d");
          new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});

      });
  </script>
  @stop