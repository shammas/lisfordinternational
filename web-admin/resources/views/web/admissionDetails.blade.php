<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
	<meta name="twitter:widgets:theme" content="light">
	<meta property="og:title" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="" />
	<meta property="og:description" content="" />
	<title>Lisford International School</title>
	<link rel="shortcut icon" type="image/x-icon" href="web/img/theme-mountain-favicon.ico">
	<!-- Css -->
	<link rel="stylesheet" href="web/css/core.min.css" />
	<link rel="stylesheet" href="web/css/skin-photo-studio.css" />
	<link rel="stylesheet" href="web/css/font-awesome.css" />
	<link rel="stylesheet" href="web/css/style.css" />
</head>
<body>
	<aside class="side-navigation-wrapper enter-right" data-no-scrollbar data-animation="reveal">
		<div class="side-navigation-scroll-pane">
			<div class="side-navigation-inner">
				<div class="side-navigation-header">
					<div class="navigation-hide side-nav-hide">
						<a href="#">
							<span class="icon-cancel medium"></span>
						</a>
					</div>
				</div>
				<nav class="side-navigation center">
					<ul>
						<li><a href="index">Home</a></li>
						<li>
							<a href="#" class="contains-sub-menu">About Us</a>
							<ul class="sub-menu">
								<li><a href="lisfordInternational">Lisford International</a></li>
								<li><a href="visionandMission">Vision and Mission</a></li>
								<li><a href="promoters">Our Promoters</a></li>
								<!-- <li><a href="prayerandSchoolSong">Prayer and School Song</a></li> -->
								<li><a href="rulesandRegulations">Rules and Regulations</a></li>
								<li><a href="sendMessage">Send Message</a></li>
							</ul>
						</li>
						<li>
							<a href="#" class="contains-sub-menu">Academics</a>
							<ul class="sub-menu">
								<li><a href="academicsEthos">Academics Ethos</a></li>
								<li><a href="Curriculum">Curriculum</a></li>
								<li><a href="addonCourses">Add-on Courses</a></li>
								<li><a href="reportingandProgress">Reporting and Progress</a></li>
								<li><a href="results">Results</a></li>
								<li><a href="library">Library</a></li>
								<li><a href="departments">Departments</a></li>
							</ul>
						</li>
						<li class="current"><a href="admissionDetails">Admission</a></li>
						<li><a href="kanfedMeritScholarship">Apply for Scholarship</a></li>
						<!-- <li>
							<a href="#" class="contains-sub-menu">Co-Curricular</a>
							<ul class="sub-menu">
								<li><a href="clubActivities">Club Activities</a></li>
								<li><a href="leadershipPrograms">Leadership Programs</a></li>
								<li><a href="simulatedMarket">Simulated Market</a></li>
								<li><a href="interactionwithEminentPersonalities">Interaction with Eminent Personalities</a></li>
								<li><a href="socialServices">Social Services</a></li>
								<li><a href="tripsandTours">Trips and Tours</a></li>
							</ul>
						</li> -->
						<li><a href="coCurricular">Co-Curricular</a></li>
						<li><a href="socialResponsibility">Social Responsibility</a></li>
						<li><a href="career">Career</a></li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="newsandEvents">News and Events</a></li>
						<li><a href="contactUs">Contact Us</a></li>

					</ul>
				</nav>
				<div class="side-navigation-footer center">
					<ul class="social-list list-horizontal center">
						<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
					</ul>
					<p class="copyright no-margin-bottom">&copy; <span id="year"></span> Lisford International School.</p>
				</div>
			</div>
		</div>
	</aside>
	<div class="wrapper reveal-side-navigation">
		<div class="wrapper-inner">
			@include('web.templates.header')
			<div class="content clearfix">
				<section class="section-block intro-title-1 xsmall bkg-vilet bbgold">
					<div class="row">
						<div class="column width-12">
							<div class="title-container">
								<div class="title-container-inner">
									<h1 class="inline color-white">Admission Details</h1>
									<ul class="breadcrumb color-white">
										<li><a class="color-white" href="index">Home</a></li>
										<li>Admission</li>
										<li>Admission Details</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-block">
					<div class="row flex">
						<div class="column width-12 v-align-middle">
							<div class="feature-column">
								<div class="feature-text">
									<h3 class="mb-30 no-padding-on-mobile">LISFORD International School</h3>
									<p class="lead">The LISFORD International School will provide admission to both boys and girls who have exceptional study records, good character, positive attitudes and talents and interests in either Science or Commerce streams.</p>
									<p class="mb-mobile-50">
										LISFORD’s prime priority will be students’ ability to cope with our academic curriculum, discipline, as well as to contribute strongly to the wider life of our school.  Apart from their academic ability and potential, LISFORD International looks for qualities in students that show they will make the very best use of the extraordinary opportunities offered here, in and out of the classroom; and students who will seek to make their mark in the school, with active and positive participation, and leave a legacy behind them. LISFORD want students not only to benefit themselves from education but also to add values to the lives of other people. LISFORD International also wants each of its students  to be leaders in their communities, their countries and around the world.
									</p>
									<p class="mb-mobile-50">
										LISFORD International will recruit outstanding students through LISFORD Admission Test (LAT) which will be conducted during January to May in every year. 
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="column width-12">
							<h3 class="mb-30 no-padding-on-mobile text-violet"><b>Fee Structure</b></h3>
							<div class="cart-review">
								<table class="table non-responsive">
									<thead>
										<tr>
											<th class="product-name">Plus One Science</th>
											<th class="product-subtotal">Amount</th>
										</tr>
									</thead>
									<tbody>
										<tr class="cart-item">
											<td class="product-name">
												<h5>First Installment</h5>
												<ul>
													<li>Reg. Fee : 1000</li>
													<li>Admission Fee: 5000</li>
													<li>Library Fee: 1000</li>
													<li>Tuition Fee: 12000</li>
												</ul>
											</td>
											<td class="product-price">
												<span class="amount">₹ 19,000.00</span>
											</td>
										</tr>
										<tr class="cart-item">
											<td class="product-name">
												<h5>Second Installment</h5>
											</td>
											<td class="product-price">
												<span class="amount">₹ 18,000.00</span>
											</td>
										</tr>
										<tr class="cart-item">
											<td class="product-name">
												<h5>Third Installment</h5>
											</td>
											<td class="product-price">
												<span class="amount">₹ 18,000.00</span>
											</td>
										</tr>
										<tr class="cart-item">
											<td class="product-name">
												<h5><b>Total Fee/Year</b></h5>
											</td>
											<td class="product-price">
												<span class="amount"><b>₹ 55,000.00</b></span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="column width-12">
							<div class="cart-review">
								<table class="table non-responsive">
									<thead>
										<tr>
											<th class="product-name">Plus One Commerce</th>
											<th class="product-subtotal">Amount</th>
										</tr>
									</thead>
									<tbody>
										<tr class="cart-item">
											<td class="product-name">
												<h5>First Installment</h5>
												<ul>
													<li>Reg. Fee : 1000</li>
													<li>Admission Fee: 5000</li>
													<li>Library Fee: 1000</li>
													<li>Tuition Fee: 10000</li>
												</ul>
											</td>
											<td class="product-price">
												<span class="amount">₹ 17,000.00</span>
											</td>
										</tr>
										<tr class="cart-item">
											<td class="product-name">
												<h5>Second Installment</h5>
											</td>
											<td class="product-price">
												<span class="amount">₹ 14,000.00</span>
											</td>
										</tr>
										<tr class="cart-item">
											<td class="product-name">
												<h5>Third Installment</h5>
											</td>
											<td class="product-price">
												<span class="amount">₹ 14,000.00</span>
											</td>
										</tr>
										<tr class="cart-item">
											<td class="product-name">
												<h5><b>Total Fee/Year</b></h5>
											</td>
											<td class="product-price">
												<span class="amount"><b>₹ 45,000.00</b></span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</section>
				<section class="section-block replicable-content pt-60 pb-30 call-to-action-2 call-to-action-2-1 bkg-vilet background-cover">
					<div class="row">
						<div class="column width-8 right center-on-mobile color-white">
							<p class="weight-light font-alt-1">We provide admission to both boys and girls, <b>Apply Now</b></p>
						</div>
						<div class="column width-4 left center-on-mobile">
							<a href="applyNow" class="button text-uppercase bkg-charcoal bkg-hover-theme color-violet color-hover-white mb-mobile-30">Go to Apply</a> 
						</div>
					</div>
				</section>
			</div>
			@include('web.templates.footer')
		</div>
		<a class="linkwhatsapp" href="https://wa.me/7994965885"><i class="fa fa-whatsapp"></i></a>
	</div>

	<!-- Js -->
	<script src="web/js/jquery.min.js"></script>
	<script src="web/js/timber.master.min.js"></script>
	<script src="web/js/custom.js"></script>
</body>
</html>