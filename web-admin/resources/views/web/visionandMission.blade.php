<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
	<meta name="twitter:widgets:theme" content="light">
	<meta property="og:title" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="" />
	<meta property="og:description" content="" />
	<title>Lisford International School</title>
	<link rel="shortcut icon" type="image/x-icon" href="web/img/theme-mountain-favicon.ico">
	<!-- Css -->
	<link rel="stylesheet" href="web/css/core.min.css" />
	<link rel="stylesheet" href="web/css/skin-photo-studio.css" />
	<link rel="stylesheet" href="web/css/font-awesome.css" />
	<link rel="stylesheet" href="web/css/style.css" />
</head>
<body>
	<aside class="side-navigation-wrapper enter-right" data-no-scrollbar data-animation="reveal">
		<div class="side-navigation-scroll-pane">
			<div class="side-navigation-inner">
				<div class="side-navigation-header">
					<div class="navigation-hide side-nav-hide">
						<a href="#">
							<span class="icon-cancel medium"></span>
						</a>
					</div>
				</div>
				<nav class="side-navigation center">
					<ul>
						<li><a href="index">Home</a></li>
						<li class="current">
							<a href="#" class="contains-sub-menu">About Us</a>
							<ul class="sub-menu">
								<li><a href="lisfordInternational">Lisford International</a></li>
								<li><a href="visionandMission">Vision and Mission</a></li>
								<li><a href="promoters">Our Promoters</a></li>
								<!-- <li><a href="prayerandSchoolSong">Prayer and School Song</a></li> -->
								<li><a href="rulesandRegulations">Rules and Regulations</a></li>
								<li><a href="sendMessage">Send Message</a></li>
							</ul>
						</li>
						<li>
							<a href="#" class="contains-sub-menu">Academics</a>
							<ul class="sub-menu">
								<li><a href="academicsEthos">Academics Ethos</a></li>
								<li><a href="Curriculum">Curriculum</a></li>
								<li><a href="addonCourses">Add-on Courses</a></li>
								<li><a href="reportingandProgress">Reporting and Progress</a></li>
								<li><a href="results">Results</a></li>
								<li><a href="library">Library</a></li>
								<li><a href="departments">Departments</a></li>
							</ul>
						</li>
						<li><a href="admissionDetails">Admission</a></li>
						<li><a href="kanfedMeritScholarship">Apply for Scholarship</a></li>
						<!-- <li>
							<a href="#" class="contains-sub-menu">Co-Curricular</a>
							<ul class="sub-menu">
								<li><a href="clubActivities">Club Activities</a></li>
								<li><a href="leadershipPrograms">Leadership Programs</a></li>
								<li><a href="simulatedMarket">Simulated Market</a></li>
								<li><a href="interactionwithEminentPersonalities">Interaction with Eminent Personalities</a></li>
								<li><a href="socialServices">Social Services</a></li>
								<li><a href="tripsandTours">Trips and Tours</a></li>
							</ul>
						</li> -->
						<li><a href="coCurricular">Co-Curricular</a></li>
						<li><a href="socialResponsibility">Social Responsibility</a></li>
						<li><a href="career">Career</a></li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="newsandEvents">News and Events</a></li>
						<li><a href="contactUs">Contact Us</a></li>

					</ul>
				</nav>
				<div class="side-navigation-footer center">
					<ul class="social-list list-horizontal center">
						<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
					</ul>
					<p class="copyright no-margin-bottom">&copy; <span id="year"></span> Lisford International School.</p>
				</div>
			</div>
		</div>
	</aside>
	<div class="wrapper reveal-side-navigation">
		<div class="wrapper-inner">
            @include('web.templates.header')
			<div class="content clearfix">
				<section class="section-block intro-title-1 xsmall bkg-vilet bbgold">
					<div class="row">
						<div class="column width-12">
							<div class="title-container">
								<div class="title-container-inner">
									<h1 class="inline color-white">Vision and Mission</h1>
									<ul class="breadcrumb color-white">
										<li><a class="color-white" href="index">Home</a></li>
										<li>About Us</li>
										<li>Vision and Mission</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-block">
					<div class="row">
						<div class="column width-8 offset-2 center">
							<h2 class="mb-30">LISFORD INTERNATIONAL SCHOOL</h2>
							<p class="lead mb-70">
								Education is the most powerful tool to better ourselves and to transform the world into a better place.
							</p>
						</div>
					</div>
					<div class="row flex">
						<div class="column width-4 v-align-middle">
							<div class="feature-content horizon" data-animate-in="preset:slideInUpShort;duration:1000;">
								<div class="feature-content-inner">
									<div class="left">
										<div class="feature-text">
											<h3>Vision</h3>
											<p class="mb-0 mb-mobile-30 lead">
												To provide outstanding education at par with the leading national and international institutions to pursue excellence globally and shape the younger generation with exceptional abilities to lead the new world.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="column width-7 offset-1 v-align-middle">
							<div class="thumbnail horizon" data-animate-in="preset:scaleIn;duration:1000;" data-threshold="0.5">
								<img src="web/img/about-2.jpg" width="800" height="500" alt=""/>
							</div>
						</div>
					</div>
					<div class="row flex">
						<div class="column width-7 v-align-middle">
							<div class="thumbnail horizon" data-animate-in="preset:scaleIn;duration:1000;" data-threshold="0.5">
								<img src="web/img/about-3.jpg" width="800" height="500" alt=""/>
							</div>
						</div>
						<div class="column width-4 offset-1  v-align-middle">
							<div class="feature-content horizon" data-animate-in="preset:slideInUpShort;duration:1000;">
								<div class="feature-content-inner">
									<div class="left">
										<div class="feature-text">
											<h3>Mission</h3>
											<p class="mb-0 mb-mobile-30 lead">
												To focus on practical aspects of education so as to make learning a meaningful and interesting experience, actively encourage collaboration with fellow institutions in the country and abroad, to imbibe in students the winner’s attitude necessary for achieving their success and guide students to set goals for turning the invisible into the visible.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row flex">
						<div class="column width-4 v-align-middle">
							<div class="feature-content horizon" data-animate-in="preset:slideInUpShort;duration:1000;">
								<div class="feature-content-inner">
									<div class="left">
										<div class="feature-text">
											<h3>Our Core Values</h3>
											<ol class="mb-0 mb-mobile-30">
												<li>Pursuit of Excellence: We will pursue excellence in everything we do, striving tenaciously to fulfill our Vision and complete our Mission</li>
												<li>Integrity, Transparency and Accountability: We will conduct all our affairs with integrity and hold ourselves accountable to Self, Society and God.</li>
												<li>Values Inculcation: We will work to inculcate high moral and ethical values amongst the students to make them responsible citizens and excellent human beings.</li>
											</ol>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="column width-7 offset-1 v-align-middle">
							<div class="thumbnail horizon" data-animate-in="preset:scaleIn;duration:1000;" data-threshold="0.5">
								<img src="web/img/about-2.jpg" width="800" height="500" alt=""/>
							</div>
						</div>
					</div>
				</section>
			</div>
            @include('web.templates.footer')
		</div>
		<a class="linkwhatsapp" href="https://wa.me/7994965885"><i class="fa fa-whatsapp"></i></a>
	</div>

	<!-- Js -->
	<script src="web/js/jquery.min.js"></script>
	<script src="web/js/timber.master.min.js"></script>
	<script src="web/js/custom.js"></script>
</body>
</html>