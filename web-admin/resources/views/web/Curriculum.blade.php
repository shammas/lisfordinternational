<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
	<meta name="twitter:widgets:theme" content="light">
	<meta property="og:title" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="" />
	<meta property="og:description" content="" />
	<title>Lisford International School</title>
	<link rel="shortcut icon" type="image/x-icon" href="web/img/theme-mountain-favicon.ico">
	<!-- Css -->
	<link rel="stylesheet" href="web/css/core.min.css" />
	<link rel="stylesheet" href="web/css/skin-photo-studio.css" />
	<link rel="stylesheet" href="web/css/font-awesome.css" />
	<link rel="stylesheet" href="web/css/style.css" />
</head>
<body>
	<aside class="side-navigation-wrapper enter-right" data-no-scrollbar data-animation="reveal">
		<div class="side-navigation-scroll-pane">
			<div class="side-navigation-inner">
				<div class="side-navigation-header">
					<div class="navigation-hide side-nav-hide">
						<a href="#">
							<span class="icon-cancel medium"></span>
						</a>
					</div>
				</div>
				<nav class="side-navigation center">
					<ul>
						<li><a href="index">Home</a></li>
						<li>
							<a href="#" class="contains-sub-menu">About Us</a>
							<ul class="sub-menu">
								<li><a href="lisfordInternational">Lisford International</a></li>
								<li><a href="visionandMission">Vision and Mission</a></li>
								<li><a href="promoters">Our Promoters</a></li>
								<!-- <li><a href="prayerandSchoolSong">Prayer and School Song</a></li> -->
								<li><a href="rulesandRegulations">Rules and Regulations</a></li>
								<li><a href="sendMessage">Send Message</a></li>
							</ul>
						</li>
						<li class="current">
							<a href="#" class="contains-sub-menu">Academics</a>
							<ul class="sub-menu">
								<li><a href="academicsEthos">Academics Ethos</a></li>
								<li><a href="Curriculum">Curriculum</a></li>
								<li><a href="addonCourses">Add-on Courses</a></li>
								<li><a href="reportingandProgress">Reporting and Progress</a></li>
								<li><a href="results">Results</a></li>
								<li><a href="library">Library</a></li>
								<li><a href="departments">Departments</a></li>
							</ul>
						</li>
						<li><a href="admissionDetails">Admission</a></li>
						<li><a href="kanfedMeritScholarship">Apply for Scholarship</a></li>
						<!-- <li>
							<a href="#" class="contains-sub-menu">Co-Curricular</a>
							<ul class="sub-menu">
								<li><a href="clubActivities">Club Activities</a></li>
								<li><a href="leadershipPrograms">Leadership Programs</a></li>
								<li><a href="simulatedMarket">Simulated Market</a></li>
								<li><a href="interactionwithEminentPersonalities">Interaction with Eminent Personalities</a></li>
								<li><a href="socialServices">Social Services</a></li>
								<li><a href="tripsandTours">Trips and Tours</a></li>
							</ul>
						</li> -->
						<li><a href="coCurricular">Co-Curricular</a></li>
						<li><a href="socialResponsibility">Social Responsibility</a></li>
						<li><a href="career">Career</a></li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="newsandEvents">News and Events</a></li>
						<li><a href="contactUs">Contact Us</a></li>

					</ul>
				</nav>
				<div class="side-navigation-footer center">
					<ul class="social-list list-horizontal center">
						<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
					</ul>
					<p class="copyright no-margin-bottom">&copy; <span id="year"></span> Lisford International School.</p>
				</div>
			</div>
		</div>
	</aside>
	<div class="wrapper reveal-side-navigation">
		<div class="wrapper-inner">
			@include('web.templates.header')
			<div class="content clearfix">
				<section class="section-block intro-title-1 xsmall bkg-vilet bbgold">
					<div class="row">
						<div class="column width-12">
							<div class="title-container">
								<div class="title-container-inner">
									<h1 class="inline color-white">Curriculum</h1>
									<ul class="breadcrumb color-white">
										<li><a class="color-white" href="index">Home</a></li>
										<li>Academics</li>
										<li>Curriculum</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-block hero-5 hero-5-2 clear-height right show-media-column-on-mobile replicable-content">
					<div class="media-column width-4"></div>
					<div class="row">
						<div class="column width-8 left">
							<p class="lead mb-30">
								LISFORD offers Kerala recognised Plus One Science and Commerce with useful add-on courses and coaching. 
							</p>
							<div class="row">
								<div class="column width-12">
									<div class="row content-grid-2">
										<div class="grid-item horizon" data-animate-in="preset:slideInLeftShort;duration:800ms;delay:100ms;" data-threshold="0.3">
											<div class="thumbnail" data-hover-easing="easeInOut" data-hover-speed="500" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="0.9">
												<img src="web/img/plus-1.jpg" width="760" height="500" alt=""/>
											</div>
											<div class="team-content-info">
												<h4 class="mb-20">Plus One Science</h4>
												<p>
													Apart from the conventional streams and focuses LISFORD focuses on special coaching to enable the students to appear for various scholarship examinations to get admission with scholarship in prestigious institutions like IISER, NISER, IISC, IIT, Central Universities, National Defense Academy, NAVY, Airforce, AFMC, Railway etc.  It will help the students to pursue their UG and PG courses without any investments and gain lucrative professions and future. 
												</p>
											</div>
										</div>
										<div class="grid-item horizon" data-animate-in="preset:slideInLeftShort;duration:800ms;delay:200ms;" data-threshold="0.3">
											<div class="thumbnail" data-hover-easing="easeInOut" data-hover-speed="500" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="0.9">
												<img src="web/img/plus-2.jpg" width="760" height="500" alt=""/>
											</div>
											<div class="team-content-info">
												<h4 class="mb-20">Plus One Commerce</h4>
												<p>
													LISFORD INTERNATIONAL offers Plus One Commerce with UK Certified add-on courses. Today the segment of Commerce has a variety of opportunities and the commerce professionals can lead their lives in new heights. During this Covid-19 and post Covid-19 time, the commerce graduates and professionals will have tremendous opportunities in India and abroad.
												</p>
												<p>
													After Plus Two commerce, the students can pursue their studies in the following areas:
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Team Grid End -->
						</div>
					</div>
				</section>
				<section class="section-block">
					<div class="row">
						<div class="column width-6">
							<h4 class="mb-50">After Plus Two commerce, the students can pursue their studies in the following areas:</h4>
							<ul>
								<li>Bachelors of Commerce (B.Com) </li>
								<li>Bachelors of Commerce (Honours) or B.com (Hons) </li>
								<li>Bachelors in Economics.</li>
								<li>Bachelors of Business Administration (BBA)</li>
								<li>Bachelor of Management Studies (BMS)</li>
								<li>Bachelor of Arts (BA)</li>
								<li>Bachelor of Fine Arts (BFA)</li>
								<li>BDes in Animation</li>
								<li>BDes in Design</li>
								<li>BSc in Hospitality & Travel</li>
								<li>BSc in Design</li>
								<li>Bachelor of Journalism & Mass Communication (BJMC)</li>
								<li>Bachelor of Mass Media (BMM)</li>
								<li>BEd</li>
							</ul>							
						</div>
						<div class="column width-6">
							<h4 class="mb-50">Professional Courses after Plus Two Commerce:</h4>
							<ul>
								<li>Chartered Accountancy (CA)</li>
								<li>Company Secretary (CS)</li>
								<li>Cost and Management Accountant (CMA)</li>
								<li>Certified Financial Planner (CFP)</li>
								<li>Bachelor of Law (LLB)</li>
							</ul>
						</div>
					</div>
				</section>
			</div>
			@include('web.templates.footer')
		</div>
		<a class="linkwhatsapp" href="https://wa.me/7994965885"><i class="fa fa-whatsapp"></i></a>
	</div>

	<!-- Js -->
	<script src="web/js/jquery.min.js"></script>
	<script src="web/js/timber.master.min.js"></script>
	<script src="web/js/custom.js"></script>
</body>
</html>