<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
	<meta name="twitter:widgets:theme" content="light">
	<meta property="og:title" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="" />
	<meta property="og:description" content="" />
	<title>Lisford International School</title>
	<link rel="shortcut icon" type="image/x-icon" href="web/img/theme-mountain-favicon.ico">
	<!-- Css -->
	<link rel="stylesheet" href="web/css/core.min.css" />
	<link rel="stylesheet" href="web/css/skin-photo-studio.css" />
	<link rel="stylesheet" href="web/css/font-awesome.css" />
	<link rel="stylesheet" href="web/css/style.css" />
</head>
<body>
	<aside class="side-navigation-wrapper enter-right" data-no-scrollbar data-animation="reveal">
		<div class="side-navigation-scroll-pane">
			<div class="side-navigation-inner">
				<div class="side-navigation-header">
					<div class="navigation-hide side-nav-hide">
						<a href="#">
							<span class="icon-cancel medium"></span>
						</a>
					</div>
				</div>
				<nav class="side-navigation center">
					<ul>
						<li><a href="index">Home</a></li>
						<li>
							<a href="#" class="contains-sub-menu">About Us</a>
							<ul class="sub-menu">
								<li><a href="lisfordInternational">Lisford International</a></li>
								<li><a href="visionandMission">Vision and Mission</a></li>
								<li><a href="promoters">Our Promoters</a></li>
								<!-- <li><a href="prayerandSchoolSong">Prayer and School Song</a></li> -->
								<li><a href="rulesandRegulations">Rules and Regulations</a></li>
								<li><a href="sendMessage">Send Message</a></li>
							</ul>
						</li>
						<li class="current">
							<a href="#" class="contains-sub-menu">Academics</a>
							<ul class="sub-menu">
								<li><a href="academicsEthos">Academics Ethos</a></li>
								<li><a href="Curriculum">Curriculum</a></li>
								<li><a href="addonCourses">Add-on Courses</a></li>
								<li><a href="reportingandProgress">Reporting and Progress</a></li>
								<li><a href="results">Results</a></li>
								<li><a href="library">Library</a></li>
								<li><a href="departments">Departments</a></li>
							</ul>
						</li>
						<li><a href="admissionDetails">Admission</a></li>
						<li><a href="kanfedMeritScholarship">Apply for Scholarship</a></li>
						<!-- <li>
							<a href="#" class="contains-sub-menu">Co-Curricular</a>
							<ul class="sub-menu">
								<li><a href="clubActivities">Club Activities</a></li>
								<li><a href="leadershipPrograms">Leadership Programs</a></li>
								<li><a href="simulatedMarket">Simulated Market</a></li>
								<li><a href="interactionwithEminentPersonalities">Interaction with Eminent Personalities</a></li>
								<li><a href="socialServices">Social Services</a></li>
								<li><a href="tripsandTours">Trips and Tours</a></li>
							</ul>
						</li> -->
						<li><a href="coCurricular">Co-Curricular</a></li>
						<li><a href="socialResponsibility">Social Responsibility</a></li>
						<li><a href="career">Career</a></li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="newsandEvents">News and Events</a></li>
						<li><a href="contactUs">Contact Us</a></li>

					</ul>
				</nav>
				<div class="side-navigation-footer center">
					<ul class="social-list list-horizontal center">
						<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
					</ul>
					<p class="copyright no-margin-bottom">&copy; <span id="year"></span> Lisford International School.</p>
				</div>
			</div>
		</div>
	</aside>
	<div class="wrapper reveal-side-navigation">
		<div class="wrapper-inner">
			@include('web.templates.header')
			<div class="content clearfix">
				<section class="section-block intro-title-1 xsmall bkg-vilet bbgold">
					<div class="row">
						<div class="column width-12">
							<div class="title-container">
								<div class="title-container-inner">
									<h1 class="inline color-white">Library</h1>
									<ul class="breadcrumb color-white">
										<li><a class="color-white" href="index">Home</a></li>
										<li>Academics</li>
										<li>Library</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>










				<!-- Under Cunstruction -->
				<div class="section-block intro-title-1 pt-110 pb-80 clear-height">
					<div class="row">
						<div class="column width-8">
							<div class="title-container">
								<div class="title-container-inner">
									<p class="title-medium font-alt-1 weight-light color-charcoal horizon" data-animate-in="preset:slideInUpShort;duration:800ms;">
										<strong>Sorry,</strong> <br>
										we're doing some work on the website.
									</p>
									<p>
										<span style="font-weight: 500;">Thank you for being patient.</span> We are doing some work on the site and will be back shortly.
									</p>
								</div>
							</div>
						</div>
						<div class="column width-4 text-center">
							<img src="web/img/light_bulb_on.png">
						</div>
					</div>
				</div>
				<!-- Under Cunstruction End -->










			</div>
			@include('web.templates.footer')
		</div>
		<a class="linkwhatsapp" href="https://wa.me/7994965885"><i class="fa fa-whatsapp"></i></a>
	</div>

	<!-- Js -->
	<script src="web/js/jquery.min.js"></script>
	<script src="web/js/timber.master.min.js"></script>
	<script src="web/js/custom.js"></script>
</body>
</html>