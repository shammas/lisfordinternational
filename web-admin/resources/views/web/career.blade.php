<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
	<meta name="twitter:widgets:theme" content="light">
	<meta property="og:title" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="" />
	<meta property="og:description" content="" />
	<title>Lisford International School</title>
	<link rel="shortcut icon" type="image/x-icon" href="web/img/theme-mountain-favicon.ico">
	<!-- Css -->
	<link rel="stylesheet" href="web/css/core.min.css" />
	<link rel="stylesheet" href="web/css/skin-photo-studio.css" />
	<link rel="stylesheet" href="web/css/font-awesome.css" />
	<link rel="stylesheet" href="web/css/style.css" />
</head>
<body>
	<aside class="side-navigation-wrapper enter-right" data-no-scrollbar data-animation="reveal">
		<div class="side-navigation-scroll-pane">
			<div class="side-navigation-inner">
				<div class="side-navigation-header">
					<div class="navigation-hide side-nav-hide">
						<a href="#">
							<span class="icon-cancel medium"></span>
						</a>
					</div>
				</div>
				<nav class="side-navigation center">
					<ul>
						<li><a href="index">Home</a></li>
						<li>
							<a href="#" class="contains-sub-menu">About Us</a>
							<ul class="sub-menu">
								<li><a href="lisfordInternational">Lisford International</a></li>
								<li><a href="visionandMission">Vision and Mission</a></li>
								<li><a href="promoters">Our Promoters</a></li>
								<!-- <li><a href="prayerandSchoolSong">Prayer and School Song</a></li> -->
								<li><a href="rulesandRegulations">Rules and Regulations</a></li>
								<li><a href="sendMessage">Send Message</a></li>
							</ul>
						</li>
						<li>
							<a href="#" class="contains-sub-menu">Academics</a>
							<ul class="sub-menu">
								<li><a href="academicsEthos">Academics Ethos</a></li>
								<li><a href="Curriculum">Curriculum</a></li>
								<li><a href="addonCourses">Add-on Courses</a></li>
								<li><a href="reportingandProgress">Reporting and Progress</a></li>
								<li><a href="results">Results</a></li>
								<li><a href="library">Library</a></li>
								<li><a href="departments">Departments</a></li>
							</ul>
						</li>
						<li><a href="admissionDetails">Admission</a></li>
						<li><a href="kanfedMeritScholarship">Apply for Scholarship</a></li>
						<!-- <li>
							<a href="#" class="contains-sub-menu">Co-Curricular</a>
							<ul class="sub-menu">
								<li><a href="clubActivities">Club Activities</a></li>
								<li><a href="leadershipPrograms">Leadership Programs</a></li>
								<li><a href="simulatedMarket">Simulated Market</a></li>
								<li><a href="interactionwithEminentPersonalities">Interaction with Eminent Personalities</a></li>
								<li><a href="socialServices">Social Services</a></li>
								<li><a href="tripsandTours">Trips and Tours</a></li>
							</ul>
						</li> -->
						<li><a href="coCurricular">Co-Curricular</a></li>
						<li><a href="socialResponsibility">Social Responsibility</a></li>
						<li class="current"><a href="career">Career</a></li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="newsandEvents">News and Events</a></li>
						<li><a href="contactUs">Contact Us</a></li>

					</ul>
				</nav>
				<div class="side-navigation-footer center">
					<ul class="social-list list-horizontal center">
						<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
					</ul>
					<p class="copyright no-margin-bottom">&copy; <span id="year"></span> Lisford International School.</p>
				</div>
			</div>
		</div>
	</aside>
	<div class="wrapper reveal-side-navigation">
		<div class="wrapper-inner">
			@include('web.templates.header')
			<div class="content clearfix">
				<section class="section-block intro-title-1 xsmall bkg-vilet bbgold">
					<div class="row">
						<div class="column width-12">
							<div class="title-container">
								<div class="title-container-inner">
									<h1 class="inline color-white">Careers</h1>
									<ul class="breadcrumb color-white">
										<li><a class="color-white" href="index">Home</a></li>
										<li>Career</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
				<div class="section-block replicable-content">
					<div class="row">
						<div class="column width-8 offset-2 center">
							<p class="lead">Those who have higher qualifications, better experiences and keen interest to work in an international scenario may apply to various posts:</p>
						</div>
					</div>
				</div>
				<section class="section-block replicable-content contact-2 no-padding-top">
					<div class="row">
						<div class="column width-8 offset-2 center">
							<h2 class="mb-30">Apply Now</h2>
							<div class="contact-form-container">
								<form class="contact-form" action="php/send-email.php" method="post" novalidate>
									<div class="row">
										<div class="column width-6">
											<input type="text" name="fname" class="form-fname form-element large" placeholder="First Name*" tabindex="1" required>
										</div>
										<div class="column width-6">
											<input type="text" name="lname" class="form-lname form-element large" placeholder="Last Name" tabindex="2">
										</div>
										<div class="column width-6">
											<input type="email" name="email" class="form-email form-element large" placeholder="Email address*" tabindex="3" required>
										</div>
										<div class="column width-6">
											<input type="text" name="number" class="form-number form-element large" placeholder="Contact Mumber" tabindex="4">
										</div>
										<div class="column width-6">
											<div class="form-select form-element large">
												<select name="options" class="form-aux" data-label="Options" tabindex="5">
													<option selected="selected" value="">Job Post For</option>
													<option value="">Category</option>
													<option value="">Category</option>
													<option value="">Category</option>
													<option value="">Category</option>
													<option value="">Category</option>
												</select>
											</div>
										</div>
										<div class="column width-6">
											<input type="text" name="number" class="form-number form-element large" placeholder="Upload CV" tabindex="4">
										</div>
										<div class="column width-6">
											<input type="text" name="honeypot" class="form-honeypot form-element large">
										</div>
									</div>
									<div class="row">
										<div class="column width-12">
											<div class="field-wrapper">
												<textarea name="message" class="form-message form-element large" placeholder="Message*" tabindex="7" required></textarea>
											</div>
										</div>
										<div class="column width-12">
											<input type="submit" value="Apply Now" class="form-submit button medium bkg-theme bkg-hover-theme color-white color-hover-white">
										</div>
									</div>
								</form>
								<div class="form-response center"></div>
							</div>
						</div>
					</div>
				</section>
			</div>
			@include('web.templates.footer')
		</div>
		<a class="linkwhatsapp" href="https://wa.me/7994965885"><i class="fa fa-whatsapp"></i></a>
	</div>

	<!-- Js -->
	<script src="web/js/jquery.min.js"></script>
	<script src="web/js/timber.master.min.js"></script>
	<script src="web/js/custom.js"></script>
</body>
</html>