<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
	<meta name="twitter:widgets:theme" content="light">
	<meta property="og:title" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="" />
	<meta property="og:description" content="" />
	<title>Lisford International School</title>
	<link rel="shortcut icon" type="image/x-icon" href="web/img/theme-mountain-favicon.ico">
	<!-- Css -->
	<link rel="stylesheet" href="web/css/core.min.css" />
	<link rel="stylesheet" href="web/css/skin-photo-studio.css" />
	<link rel="stylesheet" href="web/css/font-awesome.css" />
	<link rel="stylesheet" href="web/css/style.css" />
</head>
<body>
	<aside class="side-navigation-wrapper enter-right" data-no-scrollbar data-animation="reveal">
		<div class="side-navigation-scroll-pane">
			<div class="side-navigation-inner">
				<div class="side-navigation-header">
					<div class="navigation-hide side-nav-hide">
						<a href="#">
							<span class="icon-cancel medium"></span>
						</a>
					</div>
				</div>
				<nav class="side-navigation center">
					<ul>
						<li><a href="index">Home</a></li>
						<li class="current">
							<a href="#" class="contains-sub-menu">About Us</a>
							<ul class="sub-menu">
								<li><a href="lisfordInternational">Lisford International</a></li>
								<li><a href="visionandMission">Vision and Mission</a></li>
								<li><a href="promoters">Our Promoters</a></li>
								<!-- <li><a href="prayerandSchoolSong">Prayer and School Song</a></li> -->
								<li><a href="rulesandRegulations">Rules and Regulations</a></li>
								<li><a href="sendMessage">Send Message</a></li>
							</ul>
						</li>
						<li>
							<a href="#" class="contains-sub-menu">Academics</a>
							<ul class="sub-menu">
								<li><a href="academicsEthos">Academics Ethos</a></li>
								<li><a href="Curriculum">Curriculum</a></li>
								<li><a href="addonCourses">Add-on Courses</a></li>
								<li><a href="reportingandProgress">Reporting and Progress</a></li>
								<li><a href="results">Results</a></li>
								<li><a href="library">Library</a></li>
								<li><a href="departments">Departments</a></li>
							</ul>
						</li>
						<li><a href="admissionDetails">Admission</a></li>
						<li><a href="kanfedMeritScholarship">Apply for Scholarship</a></li>
						<!-- <li>
							<a href="#" class="contains-sub-menu">Co-Curricular</a>
							<ul class="sub-menu">
								<li><a href="clubActivities">Club Activities</a></li>
								<li><a href="leadershipPrograms">Leadership Programs</a></li>
								<li><a href="simulatedMarket">Simulated Market</a></li>
								<li><a href="interactionwithEminentPersonalities">Interaction with Eminent Personalities</a></li>
								<li><a href="socialServices">Social Services</a></li>
								<li><a href="tripsandTours">Trips and Tours</a></li>
							</ul>
						</li> -->
						<li><a href="coCurricular">Co-Curricular</a></li>
						<li><a href="socialResponsibility">Social Responsibility</a></li>
						<li><a href="career">Career</a></li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="newsandEvents">News and Events</a></li>
						<li><a href="contactUs">Contact Us</a></li>

					</ul>
				</nav>
				<div class="side-navigation-footer center">
					<ul class="social-list list-horizontal center">
						<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
					</ul>
					<p class="copyright no-margin-bottom">&copy; <span id="year"></span> Lisford International School.</p>
				</div>
			</div>
		</div>
	</aside>
	<div class="wrapper reveal-side-navigation">
		<div class="wrapper-inner">
			@include('web.templates.header')
			<div class="content clearfix">
				<section class="section-block intro-title-1 xsmall bkg-vilet bbgold">
					<div class="row">
						<div class="column width-12">
							<div class="title-container">
								<div class="title-container-inner">
									<h1 class="inline color-white">Promoters’ Profile</h1>
									<ul class="breadcrumb color-white">
										<li><a class="color-white" href="index">Home</a></li>
										<li>About Us</li>
										<li>Our Promoters</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- Under Cunstruction End -->
				<section class="section-block content-inner blog-masonry grid-container fade-in-progressively" data-layout-mode="masonry" data-grid-ratio="1.5" data-animate-resize data-animate-resize-duration="0">
					<div class="row">
						<div class="column width-12">
							<div class="row grid content-grid-4 clearfix">
								<!-- Image with content -->
								<div class="grid-item grid-sizer">
									<article class="post">
										<div class="post-media">
											<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700">
												<a class="overlay-link" href="#">
													<img src="https://via.placeholder.com/400x300" alt="Lisford International School"/>
												</a>
											</div>
										</div>
										<div class="post-content with-background mb-20">
											<h4 class="mb-10 text-violet fw700">NASIRUDDEEN ALUNGAL</h4>
											<p>
												MBA, MSc Psychology, PGDCA, PGDCG
												Trainer & Orator
												Planning Council Secretary of Bharat Sevak
												Samaj (National Development Agency,
												established in 1952 by the Planning
												Commission, Govt. of India) – 10 years
												District Program Officer for Malappuram,
												Bharat Sevak Samaj – 24 years
												Director, Aswas Counseling Centers
												Managing Director, Pigments India Ltd.
												Chairman, KANFED (Kerala Association for Non
												Formal Education and Development),
												Malappuram Dt.
												Manager Mubark English School, Manjeri
											</p>
										</div>
									</article>
								</div>
								<!-- Image with content -->
								<div class="grid-item grid-sizer">
									<article class="post">
										<div class="post-media">
											<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700">
												<a class="overlay-link" href="#">
													<img src="https://via.placeholder.com/400x300" alt="Lisford International School"/>
												</a>
											</div>
										</div>
										<div class="post-content with-background mb-20">
											<h4 class="mb-10 text-violet fw700">JABIR ANAKKAYAM</h4>
											<p>
												MA, B. Ed in sociology
												Educational Management Expert, Educational
												Consultant of Schools.
												Director, NCT Global academy.
												Associated with reputed schools as member in
												the management committee.
											</p>
										</div>
									</article>
								</div>
								<!-- Image with content -->
								<div class="grid-item grid-sizer">
									<article class="post">
										<div class="post-media">
											<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700">
												<a class="overlay-link" href="#">
													<img src="https://via.placeholder.com/400x300" alt="Lisford International School"/>
												</a>
											</div>
										</div>
										<div class="post-content with-background mb-20">
											<h4 class="mb-10 text-violet fw700">O.A VAHAB</h4>
											<p>
												Managing Director of treeG builders
												Managing Director of treeG eye care hospital
												Director of treeG Majan Eye Center Barka Oman
												Secretary of Mubarak English School Manjeri
												Ph: +91 99953 46182
											</p>
										</div>
									</article>
								</div>
								<!-- Image with content -->
								<div class="grid-item grid-sizer">
									<article class="post">
										<div class="post-media">
											<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700">
												<a class="overlay-link" href="#">
													<img src="https://via.placeholder.com/400x300" alt="Lisford International School"/>
												</a>
											</div>
										</div>
										<div class="post-content with-background mb-20">
											<h4 class="mb-10 text-violet fw700">HYDER KOTTAYIL</h4>
											<p>
												Former Branch Operations Manager, Saudi
												French Bank, Jeddah ( 29 yrs)
												Achievements/Business line of business:
												(Textile, petrol stations, supermarket, grocery
												shops, restaurants, photographic studios,
												Crockery shop)
												Run: Dr. Taha Al-Edreesi office for translation
											</p>
										</div>
									</article>
								</div>
								<!-- Image with content -->
								<div class="grid-item grid-sizer">
									<article class="post">
										<div class="post-media">
											<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700">
												<a class="overlay-link" href="#">
													<img src="https://via.placeholder.com/400x300" alt="Lisford International School"/>
												</a>
											</div>
										</div>
										<div class="post-content with-background mb-20">
											<h4 class="mb-10 text-violet fw700">SALEEM</h4>
											<p>
												MA English, MA Sociology, BEd, SET
												Teacher, Trainer, Coaching Expert
												More than 20 years of experience in Higher
												Secondary Education.
											</p>
										</div>
									</article>
								</div>
								<!-- Image with content -->
								<div class="grid-item grid-sizer">
									<article class="post">
										<div class="post-media">
											<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700">
												<a class="overlay-link" href="#">
													<img src="https://via.placeholder.com/400x300" alt="Lisford International School"/>
												</a>
											</div>
										</div>
										<div class="post-content with-background mb-20">
											<h4 class="mb-10 text-violet fw700">MOHAMED ALTHAF.KP</h4>
											<p>
												MBA, PGDHRM, PGDIM, BEd.
												Teacher, educator and administrator
												More than 20 years of teaching experience in
												educational institutions.
												10 Years of experience in management of CBSE
												school in Malappuram district
												Former Jt.secretary State Vidhya School
												Management Council, Kerala
												Ph: +91 9995123676
											</p>
										</div>
									</article>
								</div>
								<!-- Image with content -->
								<div class="grid-item grid-sizer">
									<article class="post">
										<div class="post-media">
											<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700">
												<a class="overlay-link" href="#">
													<img src="https://via.placeholder.com/400x300" alt="Lisford International School"/>
												</a>
											</div>
										</div>
										<div class="post-content with-background mb-20">
											<h4 class="mb-10 text-violet fw700">MOHAMMED POOKKELAVALAPPIL</h4>
											<p>
												Business man
											</p>
										</div>
									</article>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			@include('web.templates.footer')
		</div>
		<a class="linkwhatsapp" href="https://wa.me/7994965885"><i class="fa fa-whatsapp"></i></a>
	</div>

	<!-- Js -->
	<script src="web/js/jquery.min.js"></script>
	<script src="web/js/timber.master.min.js"></script>
	<script src="web/js/custom.js"></script>
</body>
</html>