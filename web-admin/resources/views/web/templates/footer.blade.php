<footer class="footer">
				<div class="footer-top two-columns-on-tablet">
					<div class="row flex">
						<div class="column width-3">
							<div class="widget">
								<h4 class="widget-title">Lisford International School</h4>
								<ul>
									<li><i class="fa fa-angle-double-right"></i><a href="index">Home</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="lisfordInternational">Lisford International School</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="visionandMission">Vision and Mission</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="rulesandRegulations">Rules and Regulations</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="sendMessage">Send Message</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="rulesandRegulations">Rules and Regulations</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="prayerandSchoolSong">Prayer and School Song</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="career">Career</a></li>
								</ul>
							</div>
						</div>
						<div class="column width-3">
							<div class="widget">
								<h4 class="widget-title">Quick Links</h4>
								<ul>
									<li><i class="fa fa-angle-double-right"></i><a href="academicsEthos">Academics Ethos</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="Curriculum">Curriculum</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="addonCourses">Add-on Courses</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="admissionDetails">Admission Details</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="feeStructure">Fee Structure</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="applyNow">Apply Now</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="results">Results</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="coCurricular">Co-Curricular</a></li>
								</ul>
							</div>
						</div>
						<div class="column width-3">
							<div class="widget">
								<h4 class="widget-title">More Links</h4>
								<ul>
									<li><i class="fa fa-angle-double-right"></i><a href="kanfedMeritScholarship">Apply for Scholarship</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="departments">Departments</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="library">Library</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="reportingandProgress">Reporting and Progress</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="socialResponsibility">Social Responsibility</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="gallery">Gallery</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="newsandEvents">News and Events</a></li>
									<li><i class="fa fa-angle-double-right"></i><a href="contactUs">Contact Us</a></li>
								</ul>
							</div>
						</div>
						<div class="column width-3">
							<div class="widget">
								<h4 class="widget-title">Come Visit Us</h4>
								<address>
									<i class="fa fa-map-marker"></i>Level 8,Tower 1, Umiya Business BayCessna Business Park, Kadubeesanahalli, Bangalore - 560037, Karnataka.<br/>
									<i class="fa fa-map-marker"></i>Union Bank Building, Pandikkad Road, Manjeri, Malappuram-Kerala,
								</address>
								<p class="no-margin-bottom"><i class="fa fa-phone-square"></i><a href="mailto:#">+91 7994 965 885, 7356 475 238</a></p>
								<p class="no-margin-bottom"><i class="fa fa-envelope"></i><a href="mailto:info@lisfordinternational.com">info@lisfordinternational.com</a></p>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-bottom">
					<div class="row">
						<div class="column width-12">
							<div class="footer-bottom-inner center">
								<ul class="social-list list-horizontal pull-right clear-float-on-mobile no-margin-bottom">
									<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
									<li><a href="#"><i class="fa fa-instagram"></i></a></li>
									<li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
								</ul>
								<p class="copyright pull-left clear-float-on-mobile">
									&copy; <span id="year2"></span> Lisford International is Proudly Powered by<a href="#"><img src="web/img/cloudbery.png"></a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</footer>