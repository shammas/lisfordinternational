<header class="header header-fixed-on-mobile" data-sticky-threshold="0" data-bkg-threshold="100">
				<div class="header-inner">
					<div class="row nav-bar">
						<div class="column width-12 nav-bar-inner">
							<div class="logo">
								<div class="logo-inner">
									<a href="index"><img src="web/img/logo.png" alt="Lisford International School" /></a>
								</div>
							</div>
							<nav class="navigation nav-block secondary-navigation nav-right">
								<ul>
									<li>
										<div class="v-align-middle">
											<div class="dropdowns">
												<a href="applyNow" class="button no-page-fade no-label-on-mobile small no-margin-bottom">
													<span class="fa fa-send left"></span><span>Apply Now</span>
												</a>
											</div>
										</div>
									</li>
									<li>
										<div class="v-align-middle">
											<div class="dropdowns">
												<a href="kanfedMeritScholarship" class="button no-page-fade no-label-on-mobile small no-margin-bottom">
													<span class="fa fa-university left"></span><span>Apply for Scholarship</span>
												</a>
											</div>
										</div>
									</li>
									<li>
										<div class="v-align-middle">
											<div class="dropdowns">
												<a href="Curriculum" class="button no-page-fade no-label-on-mobile small no-margin-bottom">
													<span class="fa fa-graduation-cap left"></span><span>Curriculum</span>
												</a>
											</div>
										</div>
									</li>
									<li>
										<div class="v-align-middle">
											<div class="dropdowns">
												<a href="admissionDetails" class="button no-page-fade no-label-on-mobile small no-margin-bottom">
													<span class="fa fa-file-text left"></span><span>Admission</span>
												</a>
											</div>
										</div>
									</li>
									<li class="aux-navigation">
										<a href="#" class="navigation-show side-nav-show nav-icon">
											<i class="fa fa-bars" aria-hidden="true"></i>
										</a>
									</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</header>