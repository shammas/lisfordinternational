<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
	<meta name="twitter:widgets:theme" content="light">
	<meta property="og:title" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="" />
	<meta property="og:description" content="" />
	<title>Lisford International School</title>
	<link rel="shortcut icon" type="image/x-icon" href="web/img/theme-mountain-favicon.ico">
	<!-- Css -->
	<link rel="stylesheet" href="web/css/core.min.css" />
	<link rel="stylesheet" href="web/css/skin-photo-studio.css" />
	<link rel="stylesheet" href="web/css/font-awesome.css" />
	<link rel="stylesheet" href="web/css/style.css" />
</head>
<body>
	<aside class="side-navigation-wrapper enter-right" data-no-scrollbar data-animation="reveal">
		<div class="side-navigation-scroll-pane">
			<div class="side-navigation-inner">
				<div class="side-navigation-header">
					<div class="navigation-hide side-nav-hide">
						<a href="#">
							<span class="icon-cancel medium"></span>
						</a>
					</div>
				</div>
				<nav class="side-navigation center">
					<ul>
						<li class="current"><a href="index">Home</a></li>
						<li>
							<a href="#" class="contains-sub-menu">About Us</a>
							<ul class="sub-menu">
								<li><a href="lisfordInternational">Lisford International</a></li>
								<li><a href="visionandMission">Vision and Mission</a></li>
								<li><a href="promoters">Our Promoters</a></li>
								<!-- <li><a href="prayerandSchoolSong">Prayer and School Song</a></li> -->
								<li><a href="rulesandRegulations">Rules and Regulations</a></li>
								<li><a href="sendMessage">Send Message</a></li>
							</ul>
						</li>
						<li>
							<a href="#" class="contains-sub-menu">Academics</a>
							<ul class="sub-menu">
								<li><a href="academicsEthos">Academics Ethos</a></li>
								<li><a href="Curriculum">Curriculum</a></li>
								<li><a href="addonCourses">Add-on Courses</a></li>
								<li><a href="reportingandProgress">Reporting and Progress</a></li>
								<li><a href="results">Results</a></li>
								<li><a href="library">Library</a></li>
								<li><a href="departments">Departments</a></li>
							</ul>
						</li>
						<li><a href="admissionDetails">Admission</a></li>
						<li><a href="kanfedMeritScholarship">Apply for Scholarship</a></li>
						<!-- <li>
							<a href="#" class="contains-sub-menu">Co-Curricular</a>
							<ul class="sub-menu">
								<li><a href="clubActivities">Club Activities</a></li>
								<li><a href="leadershipPrograms">Leadership Programs</a></li>
								<li><a href="simulatedMarket">Simulated Market</a></li>
								<li><a href="interactionwithEminentPersonalities">Interaction with Eminent Personalities</a></li>
								<li><a href="socialServices">Social Services</a></li>
								<li><a href="tripsandTours">Trips and Tours</a></li>
							</ul>
						</li> -->
						<li><a href="coCurricular">Co-Curricular</a></li>
						<li><a href="socialResponsibility">Social Responsibility</a></li>
						<li><a href="career">Career</a></li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="newsandEvents">News and Events</a></li>
						<li><a href="contactUs">Contact Us</a></li>

					</ul>
				</nav>
				<div class="side-navigation-footer center">
					<ul class="social-list list-horizontal center">
						<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
					</ul>
					<p class="copyright no-margin-bottom">&copy; <span id="year"></span> Lisford International School.</p>
				</div>
			</div>
		</div>
	</aside>
	<div class="wrapper reveal-side-navigation">
		<div class="wrapper-inner">
			@include('web.templates.header')
			<div class="content clearfix">
				<!-- Slider 1900px * 700px -->
				<div class="tm-slider-container full-width-slider" data-scale-under="1140" data-speed="1000">
					<ul class="tms-slides">
					    @foreach($slider as $value)
						<li class="tms-slide" data-image data-as-bkg-image data-force-fit data-animation="slideTopBottom" data-overlay-bkg-color="#000000" data-overlay-bkg-opacity="0.3">
							<img data-src="{{asset('storage/'.basename($value->image_path))}}" data-retina src="web/img/blank.png" alt=""/>
						</li>
						@endforeach
					</ul>
				</div>
				<!-- Scroll news bar -->
	            <div class="breaking-news">
	                <marquee height="20" behavior="alternate" onmouseover="this.stop();" onmouseout="this.start();">
	                @foreach($update as $key => $value)
						<span>{{$value->content}}</span>
					@endforeach
					</marquee>
	            </div>
	            <!-- About -->
				<div class="section-block replicable-content bkg-grey-ultralight">
					<div class="row">
						<div class="column width-4 left">
							<h2 class="mb-30"><span>About</span> Lisford International</h2>
							<p class="lead mb-30 text-violet">Education is the most powerful tool to better ourselves and to transform the world into a better place.</p>
							<p>
								LISFORD International has started its Higher Secondary School for both boys and girls in Manjeri, Kerala, India, aimed at laying the foundations for students to make their dreams come true with the winning combination of international quality education and values.
							</p>
							<p>
								We aim to equip our students as extraordinary to win prestigious scholarship examinations. 
							</p>
						</div>
						<div class="column width-8">
							<div class="row content-grid-2">
								<div class="grid-item horizon" data-animate-in="preset:slideInRightShort;duration:1000ms;" data-threshold="0.3">
									<div class="thumbnail no-margin-bottom" data-hover-easing="easeInOut" data-hover-speed="500" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="0.9">
										<img src="web/img/home-about-1.jpg" width="760" height="500" alt=""/>
									</div>
								</div>
								<div class="grid-item horizon" data-animate-in="preset:slideInRightShort;duration:1000ms;delay:300ms;" data-threshold="0.3">
									<div class="feature-column box small bkg-white mb-27 left mvbox">
										<span class="feature-icon fa fa-eye color-theme"></span>
										<div class="feature-text">
											<h4 class="fw500">our vision</h4>
											<p>
												To provide outstanding education at par with the leading national and international institutions
												<a class="badge" href="visionandMission">view more</a>
											</p>
										</div>
									</div>
									<div class="feature-column box small bkg-white mb-27 left mvbox">
										<span class="feature-icon fa fa-bullseye color-theme"></span>
										<div class="feature-text">
											<h4 class="fw500">our mission</h4>
											<p>
												To focus on practical aspects of education so as to make learning a meaningful and interesting
												<a class="badge" href="visionandMission">view more</a>
											</p>
										</div>
									</div>
									<div class="feature-column box small bkg-white mb-0 left mvbox">
										<span class="feature-icon fa fa-diamond color-theme"></span>
										<div class="feature-text">
											<h4 class="fw500">our values</h4>
											<p>
												Pursuit of Excellence: We will pursue excellence in everything we do, striving tenaciously to fulfill our Vision
												<a class="badge" href="visionandMission">view more</a>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Course details -->
				<div class="row">
					<div class="column width-12">
						<section class="section-block hero-5 hero-5-1 clear-height show-media-column-on-mobile bg-violet">
							<div class="media-column width-6 window-height background-none">
								<div class="tm-slider-container content-slider window-height" data-animation="slide" data-nav-arrows="false" data-nav-keyboard="false" data-auto-advance data-auto-advance-interval="4000" data-progress-bar="false" data-speed="1000" data-scale-under="0" data-width="722">
									<ul class="tms-slides">
										<li class="tms-slide" data-image data-as-bkg-image data-force-fit data-as-bkg-image data-animation="scaleOut">
											<img data-src="web/img/science-1.jpg" data-retina src="web/img/blank.png" alt=""/>
										</li>
										<li class="tms-slide" data-image data-as-bkg-image data-force-fit data-as-bkg-image data-animation="slideLeftRight">
											<img data-src="web/img/science-2.jpg" data-retina src="web/img/blank.png" alt=""/>
										</li>
									</ul>
								</div>
							</div>
							<div class="row">
								<div class="column width-6 offset-6">
									<div class="hero-content split-hero-content plus">
										<div class="hero-content-inner left center-on-mobile horizon" data-animate-in="preset:fadeIn;duration:1300ms;" data-threshold="0.5">
											<h2 class="mb-30 text-white">Plus One Science</h2>
											<p class="lead text-gold">ISER, NISER, IISC, IIT, Central Universities, National Defense Academy, NAVY, Airforce, AFMC, Railway etc</p>
											<p class="text-white">
												Apart from the conventional streams and focuses LISFORD focuses on special coaching to enable the students to appear for various scholarship examinations to get admission with scholarship in prestigious institutions like IISER, NISER, IISC, IIT, Central Universities, National Defense Academy, NAVY, Airforce, AFMC, Railway etc.  It will help the students to pursue their UG and PG courses without any investments and gain lucrative professions and future.
											</p>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section class="section-block hero-5 hero-5-1 clear-height right show-media-column-on-mobile bg-gold2">
							<div class="media-column width-6 window-height background-none">
								<div class="tm-slider-container content-slider window-height" data-animation="slide" data-nav-arrows="false" data-nav-keyboard="false" data-auto-advance data-auto-advance-interval="4000" data-progress-bar="false" data-speed="1000" data-scale-under="0" data-width="722">
									<ul class="tms-slides">
										<li class="tms-slide" data-image data-as-bkg-image data-force-fit data-as-bkg-image data-animation="slideLeftRight">
											<img data-src="web/img/commerce-1.jpg" data-retina src="web/img/blank.png" alt=""/>
										</li>
										<li class="tms-slide" data-image data-as-bkg-image data-force-fit data-as-bkg-image data-animation="slideTopBottom">
											<img data-src="web/img/commerce-2.jpg" data-retina src="web/img/blank.png" alt=""/>
										</li>
									</ul>
								</div>
							</div>
							<div class="row">
								<div class="column width-6">
									<div class="hero-content split-hero-content">
										<div class="hero-content-inner left center-on-mobile horizon plus" data-animate-in="preset:fadeIn;duration:1300ms;" data-threshold="0.5">
											<div class="color-white">
												<h2 class="mb-30 text-black">Plus One Commerce</h2>
												<p class="lead text-violet">Plus UK Certified Add-on Courses</p>
											</div>
											<p>
												Today the segment of Commerce has a variety of opportunities and the commerce professionals can lead their lives in new heights. During this Covid-19 and post Covid-19 time, the commerce graduates and professionals will have tremendous opportunities in India and abroad. After Plus Two commerce, the students can pursue their studies in the following areas: B.Com, BBA, BMS, BA, BFA, BSC, BJMC, BMM, BED.
											</p>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
				<!-- Latest News and Blog -->
				<!-- <div class="section-block content-inner blog-masonry grid-container fade-in-progressively bkg-grey-ultralight" data-layout-mode="masonry" data-grid-ratio="1.5" data-animate-resize data-animate-resize-duration="0">
					<div class="row">
						<div class="column width-12">
							<h3 class="headings">up to the minute <a href="#"><span class="fa fa-arrows-alt viewmore"></span></a></h3>
							<div class="row grid content-grid-4 clearfix">
								<div class="grid-item grid-sizer">
									<article class="post">
										<div class="post-media">
											<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700" data-hover-bkg-color="#5F296C" data-hover-bkg-opacity="0.9">
												<a class="overlay-link" href="#">
													<img src="web/img/blog-1.jpg" alt=""/>
													<span class="overlay-info">
														<span>
															<span>
																Lorem ipsum dolor sit amet, consectetur adipisicing elit,
															</span>
														</span>
													</span>
												</a>
											</div>
										</div>
										<div class="post-content with-background">
											<h2 class="post-title"><a href="#">Lorem ipsum dolor siting...</a></h2>
											<div class="post-info">
												<span class="post-date"><i class="fa fa-calendar mr-5"></i> 25 Aug 2020</span>
											</div>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
											</p>
										</div>
									</article>
								</div>
								<div class="grid-item">
									<article class="post">
										<div class="post-media">
											<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700" data-hover-bkg-color="#5F296C" data-hover-bkg-opacity="0.9">
												<a class="overlay-link" href="#">
													<img src="web/img/blog-2.jpg" alt=""/>
													<span class="overlay-info">
														<span>
															<span>
																Lorem ipsum dolor sit amet, consectetur adipisicing elit,
															</span>
														</span>
													</span>
												</a>
											</div>
										</div>
										<div class="post-content with-background">
											<h2 class="post-title"><a href="#">Lorem ipsum dolor siting...</a></h2>
											<div class="post-info">
												<span class="post-date"><i class="fa fa-calendar mr-5"></i> 20 Aug 2020</span>
											</div>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
											</p>
										</div>
									</article>
								</div>
								<div class="grid-item">
									<article class="post">
										<div class="post-media">
											<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700" data-hover-bkg-color="#5F296C" data-hover-bkg-opacity="0.9">
												<a class="overlay-link" href="#">
													<img src="web/img/blog-3.jpg" alt=""/>
													<span class="overlay-info">
														<span>
															<span>
																Lorem ipsum dolor sit amet, consectetur adipisicing elit,
															</span>
														</span>
													</span>
												</a>
											</div>
										</div>
										<div class="post-content with-background">
											<h2 class="post-title"><a href="#">Lorem ipsum dolor siting...</a></h2>
											<div class="post-info">
												<span class="post-date"><i class="fa fa-calendar mr-5"></i> 27 Jul 2020</span>
											</div>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
											</p>
										</div>
									</article>
								</div>
								<div class="grid-item">
									<article class="post">
										<div class="post-media">
											<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700" data-hover-bkg-color="#5F296C" data-hover-bkg-opacity="0.9">
												<a class="overlay-link" href="#">
													<img src="web/img/blog-4.jpg" alt=""/>
													<span class="overlay-info">
														<span>
															<span>
																Lorem ipsum dolor sit amet, consectetur adipisicing elit,
															</span>
														</span>
													</span>
												</a>
											</div>
										</div>
										<div class="post-content with-background">
											<h2 class="post-title"><a href="#">Lorem ipsum dolor siting...</a></h2>
											<div class="post-info">
												<span class="post-date"><i class="fa fa-calendar mr-5"></i> 27 Jul 2020</span>
											</div>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
											</p>
										</div>
									</article>
								</div>
							</div>
						</div>
					</div>
				</div> -->
				<!-- Testimonials -->
				<div class="section-block testimonial-5 testimonials-5-about-1">
					<div class="row">
						<h3 class="headings">Testimonials <a href="#"><span class="fa fa-arrows-alt viewmore"></span></a></h3>
					</div>
					<div id="testimonial-slider-1" class="testimonial-slider tm-slider-container">
						<ul class="tms-slides">
						@foreach($testimonial as $value)
							<li class="tms-slide" data-image>
								<div class="tms-content-scalable">
									<div class="row">
										<div class="column width-8 offset-2">
											<blockquote class="avatar center large">
												<span class="no-margin-bottom"><img data-src="{{asset('storage/'.basename($value->image_path))}}" src="{{asset('storage/'.basename($value->image_path))}}" alt=""></span>
												<cite class="mt-5 mb-20">{{'@'.$value->name}} - {{$value->designation}}.</cite>
												<p>{{$value->content}}</p>
											</blockquote>
										</div>
									</div>
								</div>
							</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
			@include('web.templates.footer')
		</div>
		<a class="linkwhatsapp" href="https://wa.me/7994965885"><i class="fa fa-whatsapp"></i></a>
	</div>

	<!-- Js -->
	<script src="web/js/jquery.min.js"></script>
	<script src="web/js/timber.master.min.js"></script>
	<script src="web/js/custom.js"></script>
</body>
</html>