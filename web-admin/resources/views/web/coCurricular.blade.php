<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
	<meta name="twitter:widgets:theme" content="light">
	<meta property="og:title" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="" />
	<meta property="og:description" content="" />
	<title>Lisford International School</title>
	<link rel="shortcut icon" type="image/x-icon" href="web/img/theme-mountain-favicon.ico">
	<!-- Css -->
	<link rel="stylesheet" href="web/css/core.min.css" />
	<link rel="stylesheet" href="web/css/skin-photo-studio.css" />
	<link rel="stylesheet" href="web/css/font-awesome.css" />
	<link rel="stylesheet" href="web/css/style.css" />
</head>
<body>
	<aside class="side-navigation-wrapper enter-right" data-no-scrollbar data-animation="reveal">
		<div class="side-navigation-scroll-pane">
			<div class="side-navigation-inner">
				<div class="side-navigation-header">
					<div class="navigation-hide side-nav-hide">
						<a href="#">
							<span class="icon-cancel medium"></span>
						</a>
					</div>
				</div>
				<nav class="side-navigation center">
					<ul>
						<li><a href="index">Home</a></li>
						<li>
							<a href="#" class="contains-sub-menu">About Us</a>
							<ul class="sub-menu">
								<li><a href="lisfordInternational">Lisford International</a></li>
								<li><a href="visionandMission">Vision and Mission</a></li>
								<li><a href="promoters">Our Promoters</a></li>
								<!-- <li><a href="prayerandSchoolSong">Prayer and School Song</a></li> -->
								<li><a href="rulesandRegulations">Rules and Regulations</a></li>
								<li><a href="sendMessage">Send Message</a></li>
							</ul>
						</li>
						<li>
							<a href="#" class="contains-sub-menu">Academics</a>
							<ul class="sub-menu">
								<li><a href="academicsEthos">Academics Ethos</a></li>
								<li><a href="Curriculum">Curriculum</a></li>
								<li><a href="coCurricular">Add-on Courses</a></li>
								<li><a href="reportingandProgress">Reporting and Progress</a></li>
								<li><a href="results">Results</a></li>
								<li><a href="library">Library</a></li>
								<li><a href="departments">Departments</a></li>
							</ul>
						</li>
						<li><a href="admissionDetails">Admission</a></li>
						<li><a href="kanfedMeritScholarship">Apply for Scholarship</a></li>
						<!-- <li>
							<a href="#" class="contains-sub-menu">Co-Curricular</a>
							<ul class="sub-menu">
								<li><a href="clubActivities">Club Activities</a></li>
								<li><a href="leadershipPrograms">Leadership Programs</a></li>
								<li><a href="simulatedMarket">Simulated Market</a></li>
								<li><a href="interactionwithEminentPersonalities">Interaction with Eminent Personalities</a></li>
								<li><a href="socialServices">Social Services</a></li>
								<li><a href="tripsandTours">Trips and Tours</a></li>
							</ul>
						</li> -->
						<li class="current"><a href="coCurricular">Co-Curricular</a></li>
						<li ><a href="socialResponsibility">Social Responsibility</a></li>
						<li><a href="career">Career</a></li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="newsandEvents">News and Events</a></li>
						<li><a href="contactUs">Contact Us</a></li>

					</ul>
				</nav>
				<div class="side-navigation-footer center">
					<ul class="social-list list-horizontal center">
						<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
					</ul>
					<p class="copyright no-margin-bottom">&copy; <span id="year"></span> Lisford International School.</p>
				</div>
			</div>
		</div>
	</aside>
	<div class="wrapper reveal-side-navigation">
		<div class="wrapper-inner">
			@include('web.templates.header')
			<div class="content clearfix">
				<section class="section-block intro-title-1 xsmall bkg-vilet bbgold">
					<div class="row">
						<div class="column width-12">
							<div class="title-container">
								<div class="title-container-inner">
									<h1 class="inline color-white">Co-Curricular</h1>
									<ul class="breadcrumb color-white">
										<li><a class="color-white" href="index">Home</a></li>
										<li>Co-Curricular</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section-block">
					<div class="row">
						<div class="column width-12">
							<h3 class="mb-50">Co-Curricular Activities</h3>
						</div>
					</div>
					<div class="row">
						<div class="column width-3">
							<div class="coCurricular">
								<img src="web/img/coCurricular-1.jpg">
								<p>Club Activities</p>
							</div>
						</div>
						<div class="column width-3">
							<div class="coCurricular">
								<img src="web/img/coCurricular-2.jpg">
								<p>Skill Development Programmes</p>
							</div>
						</div>
						<div class="column width-3">
							<div class="coCurricular">
								<img src="web/img/coCurricular-3.jpg">
								<p>Leadership Programmes</p>
							</div>
						</div>
						<div class="column width-3">
							<div class="coCurricular">
								<img src="web/img/coCurricular-4.jpg">
								<p>Social Services</p>
							</div>
						</div>
						<div class="column width-3">
							<div class="coCurricular">
								<img src="web/img/coCurricular-5.jpg">
								<p>Personality Enhancement Programmes</p>
							</div>
						</div>
						<div class="column width-3">
							<div class="coCurricular">
								<img src="web/img/coCurricular-6.jpg">
								<p>Entrepreneurship Development Programmes</p>
							</div>
						</div>
						<div class="column width-3">
							<div class="coCurricular">
								<img src="web/img/coCurricular-7.jpg">
								<p>Simulated Market for better Experience</p>
							</div>
						</div>
						<div class="column width-3">
							<div class="coCurricular">
								<img src="web/img/coCurricular-8.jpg">
								<p>Interaction with eminent personalities</p>
							</div>
						</div>
						<div class="column width-3">
							<div class="coCurricular">
								<img src="web/img/coCurricular-9.jpg">
								<p>Cultural Events and Competitions</p>
							</div>
						</div>
						<div class="column width-3">
							<div class="coCurricular">
								<img src="web/img/coCurricular-10.jpg">
								<p>Trips and Tours</p>
							</div>
						</div>
						<div class="column width-3">
							<div class="coCurricular">
								<img src="web/img/coCurricular-11.jpg">
								<p>Camps</p>
							</div>
						</div>
						<div class="column width-3">
							<div class="coCurricular">
								<img src="web/img/coCurricular-12.jpg">
								<p>Sports and Fitness Programmes</p>
							</div>
						</div>
					</div>
				</section>
			</div>
			@include('web.templates.footer')
		</div>
		<a class="linkwhatsapp" href="https://wa.me/7994965885"><i class="fa fa-whatsapp"></i></a>
	</div>

	<!-- Js -->
	<script src="web/js/jquery.min.js"></script>
	<script src="web/js/timber.master.min.js"></script>
	<script src="web/js/custom.js"></script>
</body>
</html>