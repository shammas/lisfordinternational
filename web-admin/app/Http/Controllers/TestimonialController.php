<?php

namespace App\Http\Controllers;

use App\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    public function index()
    {
        $testimonial = Testimonial::all();
        return view('pages.testimonial.list', compact('testimonial'));
    }

    public function viewForm($id=null)
    {
        if ($id == null) {
            return view('pages.testimonial.addOrEdit');
        }else{
            $testimonial = Testimonial::findOrFail($id);
            return view('pages.testimonial.addOrEdit', ['testimonial' => $testimonial]);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'content' => 'required',
            'file' => 'file|required',
        ]);

        if ($path = $request->file->store('public')) {
            $testimonial = new Testimonial($request->all());
            $testimonial->image_path = $path;
            if ($testimonial->save()) {
                return redirect()->route('admin.testimonial.list')->with(['smsg' => 'Testimonial added succefully.', 'heading' => 'Success!']);
            }
        }
        return redirect()->route('admin.testimonial.list')->with(['emsg' => 'Try Again later', 'heading' => 'Warning!']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'content' => 'required',
            'file' => 'file|required',
        ]);
        $testimonial = Testimonial::findOrFail($id);

        if (isset($request->file) ){
            if ($path = $request->file->store('public')) {
                $testimonial->image_path = $path;
            }
        }

        if ($testimonial->update($request->all())) {
            return redirect()->route('admin.testimonial.list')->with(['smsg' => 'Record Updated', 'heading' => 'Updated!']);
        }

        return redirect()->route('admin.testimonial.list')->with(['emsg' => 'Try Again later', 'heading' => 'Warning!']);
    }


    public function delete($id)
    {
        $testimonial = Testimonial::findOrFail($id);
        if (Testimonial::destroy($id)) {
            Storage::delete($testimonial->image_path);
        }
        return redirect()->back()->with(['smsg' => 'Deleted success', 'heading' => 'Deleted!']);
    }
}
