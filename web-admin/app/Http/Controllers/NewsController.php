<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Response;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        if ($request->type == 'social') {
            $news = News::where('type', 'social')->get();
        }else{
            $news = News::all();
        }
        return view('pages.news.list', compact('news'));
    }

    public function viewForm($id=null)
    {
        if ($id == null) {
            return view('pages.news.addOrEdit');
        }else{
            $news = News::findOrFail($id);
            return view('pages.news.addOrEdit', ['news' => $news]);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'heading' => 'required|max:255',
            'date' => 'required',
            'description' => 'required',
        ]);
        $news = new News($request->all());

        if (isset($request->file) ){
            if ($path = $request->file->store('public')) {
                $news->image_path = $path;
            }
        }
        if ($news->save()) {
            return redirect()->route('admin.news.list')->with(['smsg' => 'News added succefully.', 'heading' => 'Success!']);
        }

        return redirect()->route('admin.news.list')->with(['emsg' => 'Try Again later', 'heading' => 'Warning!']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'heading' => 'required|max:255',
            'date' => 'required',
            'description' => 'required',
        ]);
        $news = News::findOrFail($id);

        if (isset($request->file) ){
            if ($path = $request->file->store('public')) {
                $news->image_path = $path;
            }
        }

        if ($news->update($request->all())) {
            return redirect()->route('admin.news.list')->with(['smsg' => 'Record Updated', 'heading' => 'Updated!']);
        }

        return redirect()->route('admin.news.list')->with(['emsg' => 'Try Again later', 'heading' => 'Warning!']);
    }


    public function delete($id)
    {
        $slider = News::findOrFail($id);
        if (News::destroy($id)) {
            Storage::delete($slider->image_path);
        }
        return redirect()->back()->with(['smsg' => 'Deleted success', 'heading' => 'Deleted!']);
    }
}
