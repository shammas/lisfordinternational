<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    public function index()
    {
        $gallery = Gallery::all();
        return view('pages.gallery.list', compact('gallery'));
    }

    public function viewForm($id=null)
    {
        if ($id == null) {
            return view('pages.gallery.addOrEdit');
        }else{
            $gallery = Gallery::findOrFail($id);
            return view('pages.gallery.addOrEdit', ['gallery' => $gallery]);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'file' => 'file|required',
        ]);

        if ($path = $request->file->store('public')) {
            $gallery = new Gallery($request->all());
            $gallery->image_path = $path;
            if ($gallery->save()) {
                return redirect()->route('admin.galleries.list')->with(['smsg' => 'Gallery added succefully.', 'heading' => 'Success!']);
            }
        }
        return redirect()->route('admin.galleries.list')->with(['emsg' => 'Try Again later', 'heading' => 'Warning!']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'file' => 'file|required',
        ]);
        $gallery = Gallery::findOrFail($id);

        if (isset($request->file) ){
            if ($path = $request->file->store('public')) {
                $gallery->image_path = $path;
            }
        }

        if ($gallery->update($request->all())) {
            return redirect()->route('admin.galleries.list')->with(['smsg' => 'Record Updated', 'heading' => 'Updated!']);
        }

        return redirect()->route('admin.galleries.list')->with(['emsg' => 'Try Again later', 'heading' => 'Warning!']);
    }


    public function delete($id)
    {
        $gallery = Gallery::findOrFail($id);
        if (Gallery::destroy($id)) {
            Storage::delete($gallery->image_path);
        }
        return redirect()->back()->with(['smsg' => 'Deleted success', 'heading' => 'Deleted!']);
    }
}
