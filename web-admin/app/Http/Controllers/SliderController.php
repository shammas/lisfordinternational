<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Response;

class SliderController extends Controller
{
    public function index()
    {
        $slider = Slider::all();
        return view('pages.slider.list', compact('slider'));
    }

    public function viewForm($id=null)
    {
        if ($id == null) {
            return view('pages.slider.addOrEdit');
        }else{
            $slider = Slider::findOrFail($id);
            return view('pages.slider.addOrEdit', ['slider' => $slider]);
        }
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'file' => 'file|required',
        ]);

        if ($path = $request->file->store('public')) {
            $slider = new Slider($request->all());
            $slider->image_path = $path;
            if ($slider->save()) {
                return redirect()->route('admin.slider.list')->with(['smsg' => 'Slider added succefully.', 'heading' => 'Success!']);
            }
        }
        return redirect()->route('admin.slider.list')->with(['emsg' => 'Try Again later', 'heading' => 'Warning!']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'file' => 'file|required',
        ]);
        $slider = Slider::findOrFail($id);

        if (isset($request->file) ){
            if ($path = $request->file->store('public')) {
                $slider->image_path = $path;
            }
        }

        if ($slider->update($request->all())) {
            return redirect()->route('admin.slider.list')->with(['smsg' => 'Record Updated', 'heading' => 'Updated!']);
        }

        return redirect()->route('admin.slider.list')->with(['emsg' => 'Try Again later', 'heading' => 'Warning!']);
    }
    
    public function delete($id)
    {
        $slider = Slider::findOrFail($id);
        if (Slider::destroy($id)) {
            Storage::delete($slider->image_path);
        }
        return redirect()->back()->with(['smsg' => 'Deleted success', 'heading' => 'Deleted!']);
    }
}
