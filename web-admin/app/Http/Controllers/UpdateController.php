<?php

namespace App\Http\Controllers;

use App\Update;
use Illuminate\Http\Request;

class UpdateController extends Controller
{
    public function index()
    {
        $update = Update::all();
        return view('pages.update.list', compact('update'));
    }

    public function viewForm($id=null)
    {
        if ($id == null) {
            return view('pages.update.addOrEdit');
        }else{
            $update = Update::findOrFail($id);
            return view('pages.update.addOrEdit', ['update' => $update]);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
        ]);
        $update = new Update($request->all());

        if ($update->save()) {
            return redirect()->route('admin.update.list')->with(['smsg' => 'Update added succefully.', 'heading' => 'Success!']);
        }

        return redirect()->route('admin.update.list')->with(['emsg' => 'Try Again later', 'heading' => 'Warning!']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'content' => 'required',
        ]);
        $update = Update::findOrFail($id);
        if ($update->update($request->all())) {
            return redirect()->route('admin.update.list')->with(['smsg' => 'Record Updated', 'heading' => 'Updated!']);
        }

        return redirect()->route('admin.update.list')->with(['emsg' => 'Try Again later', 'heading' => 'Warning!']);
    }


    public function delete($id)
    {
        if (Update::destroy($id)) {
            return redirect()->route('admin.update.list')->with(['smsg' => 'Record Deleted', 'heading' => 'Deleted!']);
        }
    }
}
