<?php

namespace App\Http\Controllers;

use App\Testimonial;
use App\Gallery;
use App\Slider;
use App\Update;
use App\News;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function home()
    {
        $slider = Slider::all();
        $update = Update::all();
        $testimonial = Testimonial::all();
        return view('web.index', compact('slider','update','testimonial'));
    }

    public function applyNow()
    {
        return view('web.applyNow');
    }
    public function kanfedMeritScholarship()
    {
        return view('web.kanfedMeritScholarship');
    }
    public function Curriculum()
    {
        return view('web.Curriculum');
    }
    public function admissionDetails()
    {
        return view('web.admissionDetails');
    }
    public function lisfordInternational()
    {
        return view('web.lisfordInternational');
    }
    public function visionandMission()
    {
        return view('web.visionandMission');
    }

    public function promoters()
    {
        return view('web.promoters');
    }
    public function rulesandRegulations()
    {
        return view('web.rulesandRegulations');
    }
    public function sendMessage()
    {
        return view('web.sendMessage');
    }

    public function academicsEthos()
    {
        return view('web.academicsEthos');
    }
    public function addonCourses()
    {
        return view('web.addonCourses');
    }
    public function reportingandProgress()
    {
        return view('web.reportingandProgress');
    }
    public function results()
    {
        return view('web.results');
    }
    public function library()
    {
        return view('web.library');
    }
    public function departments()
    {
        return view('web.departments');
    }

    public function coCurricular()
    {
        return view('web.coCurricular');
    }
    public function socialResponsibility()
    {
        return view('web.socialResponsibility');
    }
    public function career()
    {
        return view('web.career');
    }
    public function gallery()
    {
        $gallery = Gallery::all();
        return view('web.gallery', compact('gallery'));
    }
    public function newsandEvents()
    {
        $news = News::all();
        foreach ($news as $key => $value) {
            $array = array("https:" , "/" , "youtu.be", "www.youtube.com" ,"watch?v=");
            $value->youtube = str_replace($array, "", $value->video_url);
        }
        return view('web.newsandEvents', compact('news'));
    }
    public function contactUs()
    {
        return view('web.contactUs');
    }
    public function prayerandSchoolSong()
    {
        return view('web.prayerandSchoolSong');
    }
    public function feeStructure()
    {
        return view('web.feeStructure');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
